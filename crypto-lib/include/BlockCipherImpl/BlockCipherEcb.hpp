#pragma once

template <typename Encryption>
class BlockCipher<CipherMode::ECB, Encryption>: public Cipher
{
public:
    using typename Cipher::Byte;
    using typename Cipher::CryptoError;

public:
    template<typename... A>
    BlockCipher(A... args)
            : m_encryption(args...)
    {}

    void encrypt(ByteArray& array) throw(CryptoError)
    {
        for(unsigned it=0; it < array.size(); it += Encryption::BlockSize)
            m_encryption.encryptBlock(&*array.begin() + it);
    }

    void decrypt(ByteArray& array) throw(CryptoError)
    {
        for(unsigned it=0; it < array.size(); it += Encryption::BlockSize)
            m_encryption.decryptBlock(&*array.begin() + it);
    }

protected:
    Encryption m_encryption;
};