#pragma once

#include <array>
#include <functional>
#include <algorithm>

template <typename Encryption>
class BlockCipher<CipherMode::CBC, Encryption>: public Cipher
{
public:
    using typename Cipher::Byte;
    using typename Cipher::CryptoError;
    typedef std::array<Byte, Encryption::BlockSize> InitVector;

public:
    template<typename... A>
    BlockCipher(const InitVector& iv, A... args)
            : m_iv(iv), m_encryption(args...)
    {
    }

    void encrypt(ByteArray& array) throw(CryptoError)
    {
        InitVector iv = m_iv;
        for(unsigned it=0; it < array.size(); it += Encryption::BlockSize)
        {
            Byte* const blockStart = &*array.begin() + it;
            std::transform(blockStart, blockStart+Encryption::BlockSize, iv.begin(), blockStart, std::bit_xor<Byte>());

            m_encryption.encryptBlock(blockStart);

            std::copy(blockStart, blockStart+Encryption::BlockSize, iv.begin());
        }
    }

    void decrypt(ByteArray& array) throw(CryptoError)
    {
        InitVector iv = m_iv;
        for(unsigned it=0; it < array.size(); it += Encryption::BlockSize)
        {
            Byte* const blockStart = &*array.begin() + it;

            InitVector oldVector = iv;
            std::copy(blockStart, blockStart+Encryption::BlockSize, iv.begin());

            m_encryption.decryptBlock(blockStart);

            std::transform(blockStart, blockStart+Encryption::BlockSize, oldVector.begin(), blockStart, std::bit_xor<Byte>());
        }
    }

protected:
    const InitVector m_iv;
    Encryption m_encryption;
};