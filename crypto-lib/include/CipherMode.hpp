#pragma once

enum class CipherMode
{
    ECB,
    CBC,
    PCBC
};