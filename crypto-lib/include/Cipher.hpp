#pragma once
#include <type_traits>
#include <vector>

class Cipher
{
public:
    typedef char Byte;
    typedef std::vector<Byte> ByteArray;

public:
    class CryptoError {};
    class KeyError: CryptoError {};

    virtual void encrypt(ByteArray& array) throw(CryptoError) = 0;
    virtual void decrypt(ByteArray& array) throw(CryptoError) = 0;
};