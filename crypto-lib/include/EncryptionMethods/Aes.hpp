#pragma once
#include <array>

#include <Cipher.hpp>

enum AesKeySize
{
    AES_128 = 128,
    AES_192 = 192,
    AES_256 = 256,
};

#include <EncryptionMethods/Aes/AesOpenSSLImpl.hpp>

template<AesKeySize KeySize>
class Aes
{
public:
    typedef Cipher::Byte Byte;
    typedef Cipher::ByteArray ByteArray;
    typedef Cipher::CryptoError CryptoError;
    typedef AesOpenSSLImpl AesImpl;

    typedef std::array<Byte, KeySize> KeyType;

public:
    static const unsigned BlockSize = 16;

public:
    Aes(const KeyType& key)
        : m_key(key), m_aesImpl(m_key.data(), KeySize)
    {}

    // Static version ecryption functions
    void encryptBlock(Byte* array) throw(CryptoError) { return m_aesImpl.encryptBlock(array); }
    void decryptBlock(Byte* array) throw(CryptoError) { return m_aesImpl.decryptBlock(array); }

protected:
    KeyType m_key;
    AesImpl m_aesImpl;
};

