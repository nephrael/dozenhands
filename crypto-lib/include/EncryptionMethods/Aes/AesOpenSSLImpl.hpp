#pragma once
#include <Dll.hpp>

class DLL_API AesOpenSSLImpl
{
    const unsigned char* m_key;
    unsigned m_keySize;
public:
    AesOpenSSLImpl(Cipher::Byte const *key, AesKeySize const &keySize)
            : m_key((unsigned char*)key), m_keySize(keySize)
    {}

    void encryptBlock(Cipher::Byte* array) throw(Cipher::CryptoError);
    void decryptBlock(Cipher::Byte* array) throw(Cipher::CryptoError);
};