#pragma once

#include <CipherMode.hpp>
#include <Cipher.hpp>

template <CipherMode M, typename Encryption>
class BlockCipher: public Cipher
{};

#include "BlockCipherImpl/BlockCipherEcb.hpp"
#include "BlockCipherImpl/BlockCipherCbc.hpp"
#include "BlockCipherImpl/BlockCipherPcbc.hpp"