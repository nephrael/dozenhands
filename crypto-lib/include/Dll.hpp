#pragma once

#ifndef DLL_EXPORT 
	#define DLL_API   __declspec( dllimport )
#else
	#define DLL_API   __declspec( dllexport )
#endif
