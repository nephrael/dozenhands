#pragma once

#include <Cipher.hpp>
#include <CipherMode.hpp>

#include <memory>
#include <algorithm>
#include <array>

enum class Algorithms
{
    ALGO_AES
};

class BuilderArray
{
public:
    BuilderArray(Cipher::ByteArray const &Data) : Data(Data) {
    }

    template<typename T, unsigned S>
    operator std::array<T, S>() const
    {
        std::array<T, S> array;
        std::copy_n(Data.begin(), S, array.begin());
        return array;
    }

private:
    const Cipher::ByteArray& Data;
};

class EncryptionFactory
{
public:
    class FactoryError {};
    class WrongKeySize: FactoryError {};

    using CipherPtr = std::unique_ptr<Cipher>;
public:
    //Full dynamic creator
    static CipherPtr createBlockCipher(Algorithms algo, unsigned keySize, CipherMode mode, const BuilderArray& key, const BuilderArray& iv);
};