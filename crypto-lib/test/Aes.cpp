#include "gtest/gtest.h"

#include "EncryptionMethods/Aes.hpp"
#include "BlockCipher.hpp"

/*int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}*/

TEST(AesECB, Aes)
{
    Aes<AES_256>::KeyType key {1,2,3,4,5,6,7,8,9,0};
    BlockCipher<CipherMode::ECB, Aes<AES_256> > cipher(key);

    Cipher::ByteArray array(256, 0);
    Cipher::ByteArray oldarray = array;

    cipher.encrypt(array);
    cipher.decrypt(array);

    GTEST_ASSERT_EQ(oldarray, array);
}

TEST(AesCBC, Aes)
{
    Aes<AES_256>::KeyType key {1,2,3,4,5,6,7,8,9,0};
    BlockCipher<CipherMode::CBC, Aes<AES_256> >::InitVector iv {32,2,35,4,3,5,34,54,5,35,4,5,4,54,5};
    BlockCipher<CipherMode::CBC, Aes<AES_256> > cipher(iv, key);

    Cipher::ByteArray array(256, 0);
    Cipher::ByteArray oldarray = array;

    cipher.encrypt(array);
    cipher.decrypt(array);

    GTEST_ASSERT_EQ(oldarray, array);
}

TEST(AesPCBC, Aes)
{
    Aes<AES_256>::KeyType key {1,2,3,4,5,6,7,8,9,0};
    BlockCipher<CipherMode::PCBC, Aes<AES_256> >::InitVector iv {32,2,35,4,3,5,34,54,5,35,4,5,4,54,5};
    BlockCipher<CipherMode::PCBC, Aes<AES_256> > cipher(iv, key);

    Cipher::ByteArray array(256, 0);
    Cipher::ByteArray oldarray = array;

    cipher.encrypt(array);
    cipher.decrypt(array);

    GTEST_ASSERT_EQ(oldarray, array);
}