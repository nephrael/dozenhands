#include <EncryptionFactory.hpp>

#include <BlockCipher.hpp>
#include <EncryptionMethods/Aes.hpp>

EncryptionFactory::CipherPtr EncryptionFactory::createBlockCipher(Algorithms algo, unsigned keySize, CipherMode mode, const BuilderArray& key, const BuilderArray& iv = Cipher::ByteArray())
{
    switch (algo)
    {
        case Algorithms::ALGO_AES:
        {
                switch (keySize)
                {
                    case AES_128:
                    {
                        switch (mode)
                        {
                            case CipherMode::ECB:
                                return CipherPtr(new BlockCipher<CipherMode::ECB, Aes<AES_128>>(key));
                            case CipherMode::CBC:
                                return CipherPtr(new BlockCipher<CipherMode::CBC, Aes<AES_128>>(iv,key));
                            case CipherMode::PCBC:
                                return CipherPtr(new BlockCipher<CipherMode::PCBC, Aes<AES_128>>(iv,key));
                        }
                    }
                    case AES_192:
                        switch (mode)
                        {
                            case CipherMode::ECB:
                                return CipherPtr(new BlockCipher<CipherMode::ECB, Aes<AES_192>>(key));
                            case CipherMode::CBC:
                                return CipherPtr(new BlockCipher<CipherMode::CBC, Aes<AES_192>>(iv,key));
                            case CipherMode::PCBC:
                                return CipherPtr(new BlockCipher<CipherMode::PCBC, Aes<AES_192>>(iv,key));
                        }
                    case AES_256:
                        switch (mode)
                        {
                            case CipherMode::ECB:
                                return CipherPtr(new BlockCipher<CipherMode::ECB, Aes<AES_256>>(key));
                            case CipherMode::CBC:
                                return CipherPtr(new BlockCipher<CipherMode::CBC, Aes<AES_256>>(iv,key));
                            case CipherMode::PCBC:
                                return CipherPtr(new BlockCipher<CipherMode::PCBC, Aes<AES_256>>(iv,key));
                        }
                    default:
                            throw WrongKeySize();
                }
                break;
        }
    }
}

