#include <EncryptionMethods/Aes.hpp>

#include <openssl/aes.h>

void AesOpenSSLImpl::encryptBlock(Cipher::Byte* array) throw(Cipher::CryptoError)
{
    unsigned char * ubytes = (unsigned char *)array;

    AES_KEY mkey;
    AES_set_encrypt_key(m_key, m_keySize, &mkey);
    AES_encrypt(ubytes, ubytes, &mkey);
}

void AesOpenSSLImpl::decryptBlock(Cipher::Byte* array) throw(Cipher::CryptoError)
{
    unsigned char * ubytes = (unsigned char *)array;

    AES_KEY mkey;
    AES_set_decrypt_key(m_key, m_keySize, &mkey);
    AES_decrypt(ubytes, ubytes, &mkey);
}