#include <cuda_runtime_api.h>
#include <cuda.h>
#include <string.h>

__device__ __constant__
unsigned char SBox[] = {0x63,	0x7c,	0x77,	0x7b,	0xf2,	
        0x6b,	0x6f,	0xc5,	0x30,	0x1,	0x67,	0x2b,	0xfe,
        0xd7,	0xab,	0x76,	0xca,	0x82,	0xc9,	0x7d,	0xfa,
        0x59,	0x47,	0xf0,	0xad,	0xd4,	0xa2,	0xaf,	0x9c,
        0xa4,	0x72,	0xc0,	0xb7,	0xfd,	0x93,	0x26,	0x36,
        0x3f,	0xf7,	0xcc,	0x34,	0xa5,	0xe5,	0xf1,	0x71,
        0xd8,	0x31,	0x15,	0x4,	0xc7,	0x23,	0xc3,	0x18,
        0x96,	0x5,	0x9a,	0x7,	0x12,	0x80,	0xe2,	0xeb,
        0x27,	0xb2,	0x75,	0x9,	0x83,	0x2c,	0x1a,	0x1b,
        0x6e,	0x5a,	0xa0,	0x52,	0x3b,	0xd6,	0xb3,	0x29,
        0xe3,	0x2f,	0x84,	0x53,	0xd1,	0x0,	0xed,	0x20,
        0xfc,	0xb1,	0x5b,	0x6a,	0xcb,	0xbe,	0x39,	0x4a,
        0x4c,	0x58,	0xcf,	0xd0,	0xef,	0xaa,	0xfb,	0x43,
        0x4d,	0x33,	0x85,	0x45,	0xf9,	0x2,	0x7f,	0x50,
        0x3c,	0x9f,	0xa8,	0x51,	0xa3,	0x40,	0x8f,	0x92,
        0x9d,	0x38,	0xf5,	0xbc,	0xb6,	0xda,	0x21,	0x10,
        0xff,	0xf3,	0xd2,	0xcd,	0xc,	0x13,	0xec,	0x5f,
        0x97,	0x44,	0x17,	0xc4,	0xa7,	0x7e,	0x3d,	0x64,
        0x5d,	0x19,	0x73,	0x60,	0x81,	0x4f,	0xdc,	0x22,
        0x2a,	0x90,	0x88,	0x46,	0xee,	0xb8,	0x14,	0xde,
        0x5e,	0xb,	0xdb,	0xe0,	0x32,	0x3a,	0xa,	0x49,
        0x6,	0x24,	0x5c,	0xc2,	0xd3,	0xac,	0x62,	0x91,
        0x95,	0xe4,	0x79,	0xe7,	0xc8,	0x37,	0x6d,	0x8d,
        0xd5,	0x4e,	0xa9,	0x6c,	0x56,	0xf4,	0xea,	0x65,
        0x7a,	0xae,	0x8,	0xba,	0x78,	0x25,	0x2e,	0x1c,
        0xa6,	0xb4,	0xc6,	0xe8,	0xdd,	0x74,	0x1f,	0x4b,
        0xbd,	0x8b,	0x8a,	0x70,	0x3e,	0xb5,	0x66,	0x48,
        0x3,	0xf6,	0xe,	0x61,	0x35,	0x57,	0xb9,	0x86,
        0xc1,	0x1d,	0x9e,	0xe1,	0xf8,	0x98,	0x11,	0x69,
        0xd9,	0x8e,	0x94,	0x9b,	0x1e,	0x87,	0xe9,	0xce,
        0x55,	0x28,	0xdf,	0x8c,	0xa1,	0x89,	0xd,	0xbf,
        0xe6,	0x42,	0x68,	0x41,	0x99,	0x2d,	0xf,	0xb0,
        0x54,	0xbb,	0x16};

#ifndef MAX_SHMEM
#define MAX_SHMEM 0x4000
#endif

#define KEY_SIZE 256
#define BLOCK_SIZE 64
#define MAX_SHMEM_FOR_DATA MAX_SHMEM - KEY_SIZE - 0x2C
#define MAX_BLOCK_COUNT MAX_SHMEM_FOR_DATA / BLOCK_SIZE

#define ROW_COUNT 4
#define COL_COUNT 4
#define LAYER_COUNT 4
#define LAYER_SIZE 16

__device__ void AddRoundKey(unsigned char* block, unsigned char *key) 
{
	for (int i = 0; i < BLOCK_SIZE; i++) {
		block[i] ^= key[i];
	}
}

//
__device__ void AddRoundSubKeyX(unsigned char* data, unsigned char *key) 
{
	data[3 ] ^= key[3 ];
	data[7 ] ^= key[7 ];
	data[11] ^= key[11];
	data[15] ^= key[15];
	data[2 ] ^= key[2 ];
	data[6 ] ^= key[6 ];
	data[10] ^= key[10];
	data[14] ^= key[14];
	data[1 ] ^= key[1 ];
	data[5 ] ^= key[5 ];
	data[9 ] ^= key[9 ];
	data[13] ^= key[13];
	data[0 ] ^= key[0 ];
	data[4 ] ^= key[4 ];
	data[8 ] ^= key[8 ];
	data[12] ^= key[12];
}

__device__ void AddRoundSubKeyY(unsigned char* data, unsigned char *key) 
{
	data[3 ] ^= key[3 ];
	data[19] ^= key[19];
	data[35] ^= key[35];
	data[51] ^= key[51];
	data[2 ] ^= key[2 ];
	data[18] ^= key[18];
	data[34] ^= key[34];
	data[50] ^= key[50];
	data[1 ] ^= key[1 ];
	data[17] ^= key[17];
	data[33] ^= key[33];
	data[49] ^= key[49];
	data[0 ] ^= key[0 ];
	data[16] ^= key[16];
	data[32] ^= key[32];
	data[48] ^= key[48];
}

__device__ void AddRoundSubKeyZ(unsigned char* data, unsigned char *key) 
{
	data[48] ^= key[48];
	data[52] ^= key[52];
	data[56] ^= key[56];
	data[60] ^= key[60];
	data[32] ^= key[32];
	data[36] ^= key[36];
	data[40] ^= key[40];
	data[44] ^= key[44];
	data[16] ^= key[16];
	data[20] ^= key[20];
	data[24] ^= key[24];
	data[28] ^= key[28];
	data[0 ] ^= key[0 ];
	data[4 ] ^= key[4 ];
	data[8 ] ^= key[8 ];
	data[12] ^= key[12];
}

///
__device__ void SubBytesX(unsigned char* data) 
{
	data[3 ] = SBox[data[3 ]];
	data[7 ] = SBox[data[7 ]];
	data[11] = SBox[data[11]];
	data[15] = SBox[data[15]];
	data[2 ] = SBox[data[2 ]];
	data[6 ] = SBox[data[6 ]];
	data[10] = SBox[data[10]];
	data[14] = SBox[data[14]];
	data[1 ] = SBox[data[1 ]];
	data[5 ] = SBox[data[5 ]];
	data[9 ] = SBox[data[9 ]];
	data[13] = SBox[data[13]];
	data[0 ] = SBox[data[0 ]];
	data[4 ] = SBox[data[4 ]];
	data[8 ] = SBox[data[8 ]];
	data[12] = SBox[data[12]];
}

__device__ void SubBytesY(unsigned char* data) 
{
	data[3 ] = SBox[data[3 ]];
	data[19] = SBox[data[19]];
	data[35] = SBox[data[35]];
	data[51] = SBox[data[51]];
	data[2 ] = SBox[data[2 ]];
	data[18] = SBox[data[18]];
	data[34] = SBox[data[34]];
	data[50] = SBox[data[50]];
	data[1 ] = SBox[data[1 ]];
	data[17] = SBox[data[17]];
	data[33] = SBox[data[33]];
	data[49] = SBox[data[49]];
	data[0 ] = SBox[data[0 ]];
	data[16] = SBox[data[16]];
	data[32] = SBox[data[32]];
	data[48] = SBox[data[48]];
}

__device__ void SubBytesZ(unsigned char* data) 
{
	data[48] = SBox[data[48]];
	data[52] = SBox[data[52]];
	data[56] = SBox[data[56]];
	data[60] = SBox[data[60]];
	data[32] = SBox[data[32]];
	data[36] = SBox[data[36]];
	data[40] = SBox[data[40]];
	data[44] = SBox[data[44]];
	data[16] = SBox[data[16]];
	data[20] = SBox[data[20]];
	data[24] = SBox[data[24]];
	data[28] = SBox[data[28]];
	data[0 ] = SBox[data[0 ]];
	data[4 ] = SBox[data[4 ]];
	data[8 ] = SBox[data[8 ]];
	data[12] = SBox[data[12]];
}

///
__device__ void MixRowsX(unsigned char* data) 
{
	char a0, a1, a2, a3;
	unsigned char b0, b1, b2, b3;
	

	a0 = data[3 ];
	a1 = data[7 ];
	a2 = data[11];
	a3 = data[15];

	b0 = (a0 >> 7 & 0x1B) ^ (a0 << 1);
	b1 = (a1 >> 7 & 0x1B) ^ (a1 << 1);
	b2 = (a2 >> 7 & 0x1B) ^ (a2 << 1);
	b3 = (a3 >> 7 & 0x1B) ^ (a3 << 1);

	data[3 ] = b0 ^ a3 ^ a2 ^ b1 ^ a1;
	data[7 ] = b1 ^ a0 ^ a3 ^ b2 ^ a2;
	data[11] = b2 ^ a1 ^ a0 ^ b3 ^ a3;
	data[15] = b3 ^ a2 ^ a1 ^ b0 ^ a0;

	
	a0 = data[2 ];
	a1 = data[6 ];
	a2 = data[10];
	a3 = data[14];

	b0 = (a0 >> 7 & 0x1B) ^ (a0 << 1);
	b1 = (a1 >> 7 & 0x1B) ^ (a1 << 1);
	b2 = (a2 >> 7 & 0x1B) ^ (a2 << 1);
	b3 = (a3 >> 7 & 0x1B) ^ (a3 << 1);

	data[2 ] = b0 ^ a3 ^ a2 ^ b1 ^ a1;
	data[6 ] = b1 ^ a0 ^ a3 ^ b2 ^ a2;
	data[10] = b2 ^ a1 ^ a0 ^ b3 ^ a3;
	data[14] = b3 ^ a2 ^ a1 ^ b0 ^ a0;

	
	a0 = data[1 ];
	a1 = data[5 ];
	a2 = data[9 ];
	a3 = data[13];

	b0 = (a0 >> 7 & 0x1B) ^ (a0 << 1);
	b1 = (a1 >> 7 & 0x1B) ^ (a1 << 1);
	b2 = (a2 >> 7 & 0x1B) ^ (a2 << 1);
	b3 = (a3 >> 7 & 0x1B) ^ (a3 << 1);

	data[1 ] = b0 ^ a3 ^ a2 ^ b1 ^ a1;
	data[5 ] = b1 ^ a0 ^ a3 ^ b2 ^ a2;
	data[9 ] = b2 ^ a1 ^ a0 ^ b3 ^ a3;
	data[13] = b3 ^ a2 ^ a1 ^ b0 ^ a0;

	
	a0 = data[0 ];
	a1 = data[4 ];
	a2 = data[8 ];
	a3 = data[12];

	b0 = (a0 >> 7 & 0x1B) ^ (a0 << 1);
	b1 = (a1 >> 7 & 0x1B) ^ (a1 << 1);
	b2 = (a2 >> 7 & 0x1B) ^ (a2 << 1);
	b3 = (a3 >> 7 & 0x1B) ^ (a3 << 1);

	data[0 ] = b0 ^ a3 ^ a2 ^ b1 ^ a1;
	data[4 ] = b1 ^ a0 ^ a3 ^ b2 ^ a2;
	data[8 ] = b2 ^ a1 ^ a0 ^ b3 ^ a3;
	data[12] = b3 ^ a2 ^ a1 ^ b0 ^ a0;
}

__device__ void MixRowsY(unsigned char* data) 
{
	char a0, a1, a2, a3;
	unsigned char b0, b1, b2, b3;
	
	a0 = data[3 ];
	a1 = data[19];
	a2 = data[35];
	a3 = data[51];

	b0 = (a0 >> 7 & 0x1B) ^ (a0 << 1);
	b1 = (a1 >> 7 & 0x1B) ^ (a1 << 1);
	b2 = (a2 >> 7 & 0x1B) ^ (a2 << 1);
	b3 = (a3 >> 7 & 0x1B) ^ (a3 << 1);

	data[3 ] = b0 ^ a3 ^ a2 ^ b1 ^ a1;
	data[19] = b1 ^ a0 ^ a3 ^ b2 ^ a2;
	data[35] = b2 ^ a1 ^ a0 ^ b3 ^ a3;
	data[51] = b3 ^ a2 ^ a1 ^ b0 ^ a0;

	a0 = data[2 ];
	a1 = data[18];
	a2 = data[34];
	a3 = data[50];

	b0 = (a0 >> 7 & 0x1B) ^ (a0 << 1);
	b1 = (a1 >> 7 & 0x1B) ^ (a1 << 1);
	b2 = (a2 >> 7 & 0x1B) ^ (a2 << 1);
	b3 = (a3 >> 7 & 0x1B) ^ (a3 << 1);

	data[2 ] = b0 ^ a3 ^ a2 ^ b1 ^ a1;
	data[18] = b1 ^ a0 ^ a3 ^ b2 ^ a2;
	data[34] = b2 ^ a1 ^ a0 ^ b3 ^ a3;
	data[50] = b3 ^ a2 ^ a1 ^ b0 ^ a0;

	a0 = data[1 ];
	a1 = data[17];
	a2 = data[33];
	a3 = data[49];

	b0 = (a0 >> 7 & 0x1B) ^ (a0 << 1);
	b1 = (a1 >> 7 & 0x1B) ^ (a1 << 1);
	b2 = (a2 >> 7 & 0x1B) ^ (a2 << 1);
	b3 = (a3 >> 7 & 0x1B) ^ (a3 << 1);

	data[1 ] = b0 ^ a3 ^ a2 ^ b1 ^ a1;
	data[17] = b1 ^ a0 ^ a3 ^ b2 ^ a2;
	data[33] = b2 ^ a1 ^ a0 ^ b3 ^ a3;
	data[49] = b3 ^ a2 ^ a1 ^ b0 ^ a0;

	a0 = data[0 ];
	a1 = data[16];
	a2 = data[32];
	a3 = data[48];

	b0 = (a0 >> 7 & 0x1B) ^ (a0 << 1);
	b1 = (a1 >> 7 & 0x1B) ^ (a1 << 1);
	b2 = (a2 >> 7 & 0x1B) ^ (a2 << 1);
	b3 = (a3 >> 7 & 0x1B) ^ (a3 << 1);

	data[0 ] = b0 ^ a3 ^ a2 ^ b1 ^ a1;
	data[16] = b1 ^ a0 ^ a3 ^ b2 ^ a2;
	data[32] = b2 ^ a1 ^ a0 ^ b3 ^ a3;
	data[48] = b3 ^ a2 ^ a1 ^ b0 ^ a0;
}

__device__ void MixRowsZ(unsigned char* data) 
{
	char a0, a1, a2, a3;
	unsigned char b0, b1, b2, b3;
	
	a0 = data[48];
	a1 = data[52];
	a2 = data[56];
	a3 = data[60];

	b0 = (a0 >> 7 & 0x1B) ^ (a0 << 1);
	b1 = (a1 >> 7 & 0x1B) ^ (a1 << 1);
	b2 = (a2 >> 7 & 0x1B) ^ (a2 << 1);
	b3 = (a3 >> 7 & 0x1B) ^ (a3 << 1);

	data[48] = b0 ^ a3 ^ a2 ^ b1 ^ a1;
	data[52] = b1 ^ a0 ^ a3 ^ b2 ^ a2;
	data[56] = b2 ^ a1 ^ a0 ^ b3 ^ a3;
	data[60] = b3 ^ a2 ^ a1 ^ b0 ^ a0;

	a0 = data[32];
	a1 = data[36];
	a2 = data[40];
	a3 = data[44];

	b0 = (a0 >> 7 & 0x1B) ^ (a0 << 1);
	b1 = (a1 >> 7 & 0x1B) ^ (a1 << 1);
	b2 = (a2 >> 7 & 0x1B) ^ (a2 << 1);
	b3 = (a3 >> 7 & 0x1B) ^ (a3 << 1);

	data[32] = b0 ^ a3 ^ a2 ^ b1 ^ a1;
	data[36] = b1 ^ a0 ^ a3 ^ b2 ^ a2;
	data[40] = b2 ^ a1 ^ a0 ^ b3 ^ a3;
	data[44] = b3 ^ a2 ^ a1 ^ b0 ^ a0;

	a0 = data[16];
	a1 = data[20];
	a2 = data[24];
	a3 = data[28];

	b0 = (a0 >> 7 & 0x1B) ^ (a0 << 1);
	b1 = (a1 >> 7 & 0x1B) ^ (a1 << 1);
	b2 = (a2 >> 7 & 0x1B) ^ (a2 << 1);
	b3 = (a3 >> 7 & 0x1B) ^ (a3 << 1);

	data[16] = b0 ^ a3 ^ a2 ^ b1 ^ a1;
	data[20] = b1 ^ a0 ^ a3 ^ b2 ^ a2;
	data[24] = b2 ^ a1 ^ a0 ^ b3 ^ a3;
	data[28] = b3 ^ a2 ^ a1 ^ b0 ^ a0;

	a0 = data[0 ];
	a1 = data[4 ];
	a2 = data[8 ];
	a3 = data[12];

	b0 = (a0 >> 7 & 0x1B) ^ (a0 << 1);
	b1 = (a1 >> 7 & 0x1B) ^ (a1 << 1);
	b2 = (a2 >> 7 & 0x1B) ^ (a2 << 1);
	b3 = (a3 >> 7 & 0x1B) ^ (a3 << 1);

	data[0 ] = b0 ^ a3 ^ a2 ^ b1 ^ a1;
	data[4 ] = b1 ^ a0 ^ a3 ^ b2 ^ a2;
	data[8 ] = b2 ^ a1 ^ a0 ^ b3 ^ a3;
	data[12] = b3 ^ a2 ^ a1 ^ b0 ^ a0;
}
///

__device__ void MixColsX(unsigned char* data) 
{
	char a0, a1, a2, a3;
	unsigned char b0, b1, b2, b3;
	
	{
		a0 = data[15];
		a1 = data[14];
		a2 = data[13];
		a3 = data[12];

		b0 = (a0 >> 7 & 0x1B) ^ (a0 << 1);
		b1 = (a1 >> 7 & 0x1B) ^ (a1 << 1);
		b2 = (a2 >> 7 & 0x1B) ^ (a2 << 1);
		b3 = (a3 >> 7 & 0x1B) ^ (a3 << 1);

		data[15] = b0 ^ a3 ^ a2 ^ b1 ^ a1;
		data[14] = b1 ^ a0 ^ a3 ^ b2 ^ a2;
		data[13] = b2 ^ a1 ^ a0 ^ b3 ^ a3;
		data[12] = b3 ^ a2 ^ a1 ^ b0 ^ a0;
	}

	{
		a0 = data[11];
		a1 = data[10];
		a2 = data[9 ];
		a3 = data[8 ];

		b0 = (a0 >> 7 & 0x1B) ^ (a0 << 1);
		b1 = (a1 >> 7 & 0x1B) ^ (a1 << 1);
		b2 = (a2 >> 7 & 0x1B) ^ (a2 << 1);
		b3 = (a3 >> 7 & 0x1B) ^ (a3 << 1);

		data[11] = b0 ^ a3 ^ a2 ^ b1 ^ a1;
		data[10] = b1 ^ a0 ^ a3 ^ b2 ^ a2;
		data[ 9] = b2 ^ a1 ^ a0 ^ b3 ^ a3;
		data[ 8] = b3 ^ a2 ^ a1 ^ b0 ^ a0;
	}

	{
		a0 = data[7];
		a1 = data[6];
		a2 = data[5];
		a3 = data[4];

		b0 = (a0 >> 7 & 0x1B) ^ (a0 << 1);
		b1 = (a1 >> 7 & 0x1B) ^ (a1 << 1);
		b2 = (a2 >> 7 & 0x1B) ^ (a2 << 1);
		b3 = (a3 >> 7 & 0x1B) ^ (a3 << 1);

		data[7] = b0 ^ a3 ^ a2 ^ b1 ^ a1;
		data[6] = b1 ^ a0 ^ a3 ^ b2 ^ a2;
		data[5] = b2 ^ a1 ^ a0 ^ b3 ^ a3;
		data[4] = b3 ^ a2 ^ a1 ^ b0 ^ a0;
	}

	{
		a0 = data[3];
		a1 = data[2];
		a2 = data[1];
		a3 = data[0];

		b0 = (a0 >> 7 & 0x1B) ^ (a0 << 1);
		b1 = (a1 >> 7 & 0x1B) ^ (a1 << 1);
		b2 = (a2 >> 7 & 0x1B) ^ (a2 << 1);
		b3 = (a3 >> 7 & 0x1B) ^ (a3 << 1);

		data[3] = b0 ^ a3 ^ a2 ^ b1 ^ a1;
		data[2] = b1 ^ a0 ^ a3 ^ b2 ^ a2;
		data[1] = b2 ^ a1 ^ a0 ^ b3 ^ a3;
		data[0] = b3 ^ a2 ^ a1 ^ b0 ^ a0;
	}
}

__device__ void MixColsY(unsigned char* data) 
{
	char a0, a1, a2, a3;
	unsigned char b0, b1, b2, b3;
	
	{
		a0 = data[51];
		a1 = data[50];
		a2 = data[49];
		a3 = data[48];

		b0 = (a0 >> 7 & 0x1B) ^ (a0 << 1);
		b1 = (a1 >> 7 & 0x1B) ^ (a1 << 1);
		b2 = (a2 >> 7 & 0x1B) ^ (a2 << 1);
		b3 = (a3 >> 7 & 0x1B) ^ (a3 << 1);

		data[51] = b0 ^ a3 ^ a2 ^ b1 ^ a1;
		data[50] = b1 ^ a0 ^ a3 ^ b2 ^ a2;
		data[49] = b2 ^ a1 ^ a0 ^ b3 ^ a3;
		data[48] = b3 ^ a2 ^ a1 ^ b0 ^ a0;
	}

	{
		a0 = data[35];
		a1 = data[34];
		a2 = data[33];
		a3 = data[32];

		b0 = (a0 >> 7 & 0x1B) ^ (a0 << 1);
		b1 = (a1 >> 7 & 0x1B) ^ (a1 << 1);
		b2 = (a2 >> 7 & 0x1B) ^ (a2 << 1);
		b3 = (a3 >> 7 & 0x1B) ^ (a3 << 1);

		data[35] = b0 ^ a3 ^ a2 ^ b1 ^ a1;
		data[34] = b1 ^ a0 ^ a3 ^ b2 ^ a2;
		data[33] = b2 ^ a1 ^ a0 ^ b3 ^ a3;
		data[32] = b3 ^ a2 ^ a1 ^ b0 ^ a0;
	}

	{
		a0 = data[19];
		a1 = data[18];
		a2 = data[17];
		a3 = data[16];

		b0 = (a0 >> 7 & 0x1B) ^ (a0 << 1);
		b1 = (a1 >> 7 & 0x1B) ^ (a1 << 1);
		b2 = (a2 >> 7 & 0x1B) ^ (a2 << 1);
		b3 = (a3 >> 7 & 0x1B) ^ (a3 << 1);

		data[19] = b0 ^ a3 ^ a2 ^ b1 ^ a1;
		data[18] = b1 ^ a0 ^ a3 ^ b2 ^ a2;
		data[17] = b2 ^ a1 ^ a0 ^ b3 ^ a3;
		data[16] = b3 ^ a2 ^ a1 ^ b0 ^ a0;
	}

	{
		a0 = data[3];
		a1 = data[2];
		a2 = data[1];
		a3 = data[0];

		b0 = (a0 >> 7 & 0x1B) ^ (a0 << 1);
		b1 = (a1 >> 7 & 0x1B) ^ (a1 << 1);
		b2 = (a2 >> 7 & 0x1B) ^ (a2 << 1);
		b3 = (a3 >> 7 & 0x1B) ^ (a3 << 1);

		data[3] = b0 ^ a3 ^ a2 ^ b1 ^ a1;
		data[2] = b1 ^ a0 ^ a3 ^ b2 ^ a2;
		data[1] = b2 ^ a1 ^ a0 ^ b3 ^ a3;
		data[0] = b3 ^ a2 ^ a1 ^ b0 ^ a0;
	}
}

__device__ void MixColsZ(unsigned char* data) 
{
	char a0, a1, a2, a3;
	unsigned char b0, b1, b2, b3;
	
	{
		a0 = data[60];
		a1 = data[44];
		a2 = data[28];
		a3 = data[12];

		b0 = (a0 >> 7 & 0x1B) ^ (a0 << 1);
		b1 = (a1 >> 7 & 0x1B) ^ (a1 << 1);
		b2 = (a2 >> 7 & 0x1B) ^ (a2 << 1);
		b3 = (a3 >> 7 & 0x1B) ^ (a3 << 1);

		data[60] = b0 ^ a3 ^ a2 ^ b1 ^ a1;
		data[44] = b1 ^ a0 ^ a3 ^ b2 ^ a2;
		data[28] = b2 ^ a1 ^ a0 ^ b3 ^ a3;
		data[12] = b3 ^ a2 ^ a1 ^ b0 ^ a0;
	}

	{
		a0 = data[56];
		a1 = data[40];
		a2 = data[24];
		a3 = data[8 ];

		b0 = (a0 >> 7 & 0x1B) ^ (a0 << 1);
		b1 = (a1 >> 7 & 0x1B) ^ (a1 << 1);
		b2 = (a2 >> 7 & 0x1B) ^ (a2 << 1);
		b3 = (a3 >> 7 & 0x1B) ^ (a3 << 1);

		data[56] = b0 ^ a3 ^ a2 ^ b1 ^ a1;
		data[40] = b1 ^ a0 ^ a3 ^ b2 ^ a2;
		data[24] = b2 ^ a1 ^ a0 ^ b3 ^ a3;
		data[8 ] = b3 ^ a2 ^ a1 ^ b0 ^ a0;
	}

	{
		a0 = data[52];
		a1 = data[36];
		a2 = data[20];
		a3 = data[4 ];

		b0 = (a0 >> 7 & 0x1B) ^ (a0 << 1);
		b1 = (a1 >> 7 & 0x1B) ^ (a1 << 1);
		b2 = (a2 >> 7 & 0x1B) ^ (a2 << 1);
		b3 = (a3 >> 7 & 0x1B) ^ (a3 << 1);

		data[52] = b0 ^ a3 ^ a2 ^ b1 ^ a1;
		data[36] = b1 ^ a0 ^ a3 ^ b2 ^ a2;
		data[20] = b2 ^ a1 ^ a0 ^ b3 ^ a3;
		data[4 ] = b3 ^ a2 ^ a1 ^ b0 ^ a0;
	}

	{
		a0 = data[48];
		a1 = data[32];
		a2 = data[16];
		a3 = data[0 ];

		b0 = (a0 >> 7 & 0x1B) ^ (a0 << 1);
		b1 = (a1 >> 7 & 0x1B) ^ (a1 << 1);
		b2 = (a2 >> 7 & 0x1B) ^ (a2 << 1);
		b3 = (a3 >> 7 & 0x1B) ^ (a3 << 1);

		data[48] = b0 ^ a3 ^ a2 ^ b1 ^ a1;
		data[32] = b1 ^ a0 ^ a3 ^ b2 ^ a2;
		data[16] = b2 ^ a1 ^ a0 ^ b3 ^ a3;
		data[0 ] = b3 ^ a2 ^ a1 ^ b0 ^ a0;
	}
}

__device__ void MixLayersX(unsigned char* layer, unsigned char *key) 
{
	{
		unsigned char* loffset = layer;
		unsigned char* lkoffset = key;

		SubBytesX(loffset);
		MixRowsX(loffset);
		MixColsX(loffset);
		AddRoundSubKeyX(loffset, lkoffset);
	}
	{
		unsigned char* loffset = layer + 16;
		unsigned char* lkoffset = key + 16;

		SubBytesX(loffset);
		MixRowsX(loffset);
		MixColsX(loffset);
		AddRoundSubKeyX(loffset, lkoffset);
	}
	{
		unsigned char* loffset = layer + 32;
		unsigned char* lkoffset = key + 32;

		SubBytesX(loffset);
		MixRowsX(loffset);
		MixColsX(loffset);
		AddRoundSubKeyX(loffset, lkoffset);
	}
	{
		unsigned char* loffset = layer + 48;
		unsigned char* lkoffset = key + 48;

		SubBytesX(loffset);
		MixRowsX(loffset);
		MixColsX(loffset);
		AddRoundSubKeyX(loffset, lkoffset);
	}
}

__device__ void MixLayersY(unsigned char* layer, unsigned char *key) 
{
	{
		unsigned char* loffset = layer;
		unsigned char* lkoffset = key;

		SubBytesY(loffset);
		MixRowsY(loffset);
		MixColsY(loffset);
		AddRoundSubKeyY(loffset, lkoffset);
	}
	{
		unsigned char* loffset = layer + 4;
		unsigned char* lkoffset = key + 4;

		SubBytesY(loffset);
		MixRowsY(loffset);
		MixColsY(loffset);
		AddRoundSubKeyY(loffset, lkoffset);
	}
	{
		unsigned char* loffset = layer + 8;
		unsigned char* lkoffset = key + 8;

		SubBytesY(loffset);
		MixRowsY(loffset);
		MixColsY(loffset);
		AddRoundSubKeyY(loffset, lkoffset);
	}
	{
		unsigned char* loffset = layer + 12;
		unsigned char* lkoffset = key + 12;

		SubBytesY(loffset);
		MixRowsY(loffset);
		MixColsY(loffset);
		AddRoundSubKeyY(loffset, lkoffset);
	}

}

__device__ void MixLayersZ(unsigned char* layer, unsigned char *key) 
{
	{
		unsigned char* loffset = layer;
		unsigned char* lkoffset = key;

		SubBytesZ(loffset);
		MixRowsZ(loffset);
		MixColsZ(loffset);
		AddRoundSubKeyZ(loffset, lkoffset);
	}
	{
		unsigned char* loffset = layer + 1;
		unsigned char* lkoffset = key + 1;

		SubBytesZ(loffset);
		MixRowsZ(loffset);
		MixColsZ(loffset);
		AddRoundSubKeyZ(loffset, lkoffset);
	}
	{
		unsigned char* loffset = layer + 2;
		unsigned char* lkoffset = key + 2;

		SubBytesZ(loffset);
		MixRowsZ(loffset);
		MixColsZ(loffset);
		AddRoundSubKeyZ(loffset, lkoffset);
	}
	{
		unsigned char* loffset = layer + 3;
		unsigned char* lkoffset = key + 3;

		SubBytesZ(loffset);
		MixRowsZ(loffset);
		MixColsZ(loffset);
		AddRoundSubKeyZ(loffset, lkoffset);
	}
}

extern "C"
__global__ void DozenKernelEcb(unsigned char *in, unsigned int block_count, unsigned char *key)
{	
	int block_num = blockIdx.x * blockDim.x + threadIdx.x;
	int thread_num = threadIdx.x;

	if(block_num > block_count)
		return;

	__shared__ unsigned char sh_key[KEY_SIZE];
	__shared__ unsigned char sh_in[MAX_SHMEM_FOR_DATA];
	
	unsigned char* native_block_ptr = in + block_num * BLOCK_SIZE;
	unsigned char* shared_block_ptr = sh_in + thread_num * BLOCK_SIZE;
	
	if (thread_num < BLOCK_SIZE)
            memcpy(sh_key + thread_num*4, key + thread_num*4, 4);

	memcpy(shared_block_ptr, native_block_ptr, BLOCK_SIZE);

        AddRoundKey(shared_block_ptr, sh_key);
        MixLayersX (shared_block_ptr, sh_key + 64);
        MixLayersY (shared_block_ptr, sh_key + 128);
        MixLayersZ (shared_block_ptr, sh_key + 192);

	memcpy(native_block_ptr, shared_block_ptr, BLOCK_SIZE);
}

extern "C"
__global__ void DozenKernelCtr(unsigned char *in, unsigned int block_count, unsigned char *key, unsigned int counter_offset)
{	
	int block_num = blockIdx.x * blockDim.x + threadIdx.x;
	int thread_num = threadIdx.x;

	if(block_num > block_count)
            return;

	__shared__ unsigned char sh_key[KEY_SIZE];
	__shared__ unsigned char sh_in[MAX_SHMEM_FOR_DATA];
	
	unsigned char* native_block_ptr = in + block_num * BLOCK_SIZE;
	unsigned char* shared_block_ptr = sh_in + thread_num * BLOCK_SIZE;
	
	memcpy(sh_key, key, KEY_SIZE);

	for(unsigned i=0; i<BLOCK_SIZE; ++i)
		shared_block_ptr[i] = counter_offset + block_num*BLOCK_SIZE + i;

	AddRoundKey(shared_block_ptr, sh_key);
	MixLayersX (shared_block_ptr, sh_key + 64);
	MixLayersY (shared_block_ptr, sh_key + 128);
	MixLayersZ (shared_block_ptr, sh_key + 192);

	for(unsigned i=0; i<BLOCK_SIZE; ++i)
            native_block_ptr[i] ^= shared_block_ptr[i];
}