/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package worknode;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import worknode.AbstractBusinessLogic.AbstractBusinessLogic;
import worknode.AbstractBusinessLogic.ReplyPackage;
import worknode.AbstractBusinessLogic.Request;


public class WorkNodeBusinessLogic implements AbstractBusinessLogic {


    enum RequestTypes {INFO, PUT, GET, PUTUN, TOTALCRYPT, DUMP};
    
    final Encryptor encryptor = new Encryptor(new Storage());
    
    @Override
    public ReplyPackage handling(Request info)
    {
        try {
            Byte request = info.get(Byte.class ,Request.Field.REQUEST);
            
            switch(RequestTypes.values()[request])
            {
                case PUT: return handlePut(info);
                case GET: return handleGet(info);
                case PUTUN: return handlePutUncrypted(info);
                case TOTALCRYPT: return handleTotalCrypt(info);
                case DUMP: return handleDump(info);
            }
            
        } 
        catch (Request.ObjectNotFound ex)
        {
            Logger.getLogger(WorkNodeBusinessLogic.class.getName()).log(Level.SEVERE, null, ex);
            
            ReplyPackage reply = new ReplyPackage();
            reply.set(ReplyPackage.Field.ERROR, ex.getMessage());
            return reply;
        }
        
        ReplyPackage reply = new ReplyPackage();
        reply.set(ReplyPackage.Field.ERROR, "Unknown command");
        return reply;
    }

    private ReplyPackage handlePut(Request info) throws Request.ObjectNotFound 
    {    
        ByteBuffer array = info.get(ByteBuffer.class, Request.Field.OBJECT);
        ReplyPackage reply = new ReplyPackage();
        try
        {
            UUID id = encryptor.put(array);
            reply.set(ReplyPackage.Field.OBJECT_ID, id); 
        } 
        catch (IllegalStateException ex) {
            Logger.getLogger(WorkNodeBusinessLogic.class.getName()).log(Level.SEVERE, "Could not create object", ex);
            reply.set(ReplyPackage.Field.ERROR, 1);
            reply.set(ReplyPackage.Field.ERROR_DESC, "Could not create object");
        }
        
        
        return reply;        
    }

    private ReplyPackage handleGet(Request info) throws Request.ObjectNotFound
    {
        UUID id = info.get(UUID.class, Request.Field.OBJECT_ID);
        ReplyPackage reply = new ReplyPackage();
        try
        {
            byte[] encrypt = encryptor.get(id);
            reply.set(ReplyPackage.Field.OBJECT, encrypt);
        
        } catch (IllegalStateException | OutOfMemoryError  ex) {
            Logger.getLogger(WorkNodeBusinessLogic.class.getName()).log(Level.SEVERE, "Could not create object", ex);
            reply.set(ReplyPackage.Field.ERROR, 1);
            reply.set(ReplyPackage.Field.ERROR_DESC, "Could not create object");
        }
        
        return reply;
    }

    private ReplyPackage handlePutUncrypted(Request info) throws Request.ObjectNotFound {
        ByteBuffer array = info.get(ByteBuffer.class, Request.Field.OBJECT);
        ReplyPackage reply = new ReplyPackage();        
        
        try
        {
            UUID id = encryptor.putUnEnecrypted(array);
            reply.set(ReplyPackage.Field.OBJECT_ID, id); 
        } 
        catch (IllegalStateException | OutOfMemoryError  ex) {
            Logger.getLogger(WorkNodeBusinessLogic.class.getName()).log(Level.SEVERE, "Could not create object", ex);
            reply.set(ReplyPackage.Field.ERROR, 1);
            reply.set(ReplyPackage.Field.ERROR_DESC, "Could not create object");
        }
        
        return reply; 
    }

    private ReplyPackage handleTotalCrypt(Request info) throws Request.ObjectNotFound {
        ReplyPackage reply = new ReplyPackage();        
        try
        {
            UUID id = encryptor.totalCrypt();
            reply.set(ReplyPackage.Field.OBJECT_ID, id); 
        } 
        catch (IllegalStateException | OutOfMemoryError ex) {
            Logger.getLogger(WorkNodeBusinessLogic.class.getName()).log(Level.SEVERE, "Could not create object", ex);
            reply.set(ReplyPackage.Field.ERROR, 1);
            reply.set(ReplyPackage.Field.ERROR_DESC, "Could not create object");
        }
        
        return reply; 
    }
    
    private ReplyPackage handleDump(Request info) {
        ReplyPackage reply = new ReplyPackage();
        try {
            FileChannel file = new RandomAccessFile(UUID.randomUUID().toString() + ".db", "rw").getChannel();
            try {
                encryptor.dumpAll(file);
            } catch (Exception e) {
                Logger.getLogger(WorkNodeBusinessLogic.class.getName()).log(Level.SEVERE, null, e);
            } finally {
                file.close();
            }
            
        } catch (IOException ex) {
            Logger.getLogger(WorkNodeBusinessLogic.class.getName()).log(Level.SEVERE, null, ex);
        }
        return reply;
    }
    
}
