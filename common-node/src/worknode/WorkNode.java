/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package worknode;

import worknode.TcpServer.TcpServer;
import java.io.IOException;
import java.net.Inet4Address;
import worknode.ProtocolsHandler.SuperMapProtocol;

/**
 *
 * @author d.kuzminskiy
 */
public class WorkNode {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        TcpServer server = new TcpServer(Inet4Address.getLoopbackAddress(), 9999, new SuperMapProtocol(), new WorkNodeBusinessLogic());
        server.start();
        
        System.in.read();
    }
}
