/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package worknode.ProtocolsHandler;

import java.io.IOException;
import java.nio.ByteBuffer;
import worknode.AbstractBusinessLogic.ReplyPackage;
import worknode.AbstractBusinessLogic.Request;

/**
 *
 * @author d.kuzminskiy
 */
public interface AbstractProtocol {
    
    public class ParsingException extends Exception
    {
        public ParsingException(String message) {
            super(message);
        }
    }
    
    public ByteBuffer serialize(ReplyPackage reply) throws IOException;
    public Request deserialize(ByteBuffer buffer) throws ParsingException;
}
