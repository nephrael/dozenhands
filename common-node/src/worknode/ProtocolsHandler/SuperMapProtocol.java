/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package worknode.ProtocolsHandler;

import java.io.IOException;
import java.nio.ByteBuffer;
import worknode.AbstractBusinessLogic.ReplyPackage;
import worknode.AbstractBusinessLogic.Request;


public class SuperMapProtocol implements AbstractProtocol {
    
    static enum FieldType {INT, DOUBLE, STRING, BYTEARRAY, LONG, BYTE, UUID};
    static FieldType[] values = FieldType.values();
    static Request.Field[] fields = Request.Field.values();

    @Override
    public Request deserialize(ByteBuffer buffer) throws ParsingException {
        buffer.rewind();
        
        Request pack = new Request(buffer);
        
        while(buffer.hasRemaining())
        {
            short fieldtype = buffer.get();
            short valuetype = buffer.get();
            
            if (fieldtype < 0 || fieldtype > fields.length)
                throw new ParsingException("Wrong field type" + fieldtype);
            
            if (valuetype < 0 || valuetype > values.length)
                throw new ParsingException("Wrong type: " + valuetype);
            
            switch(values[valuetype])
            {
                case INT: 
                    pack.set(fields[fieldtype], buffer.position());
                    buffer.position(buffer.position() + 4);
                    break;
                    
                case DOUBLE:
                    pack.set(fields[fieldtype], buffer.position());
                    buffer.position(buffer.position() + 8);
                    break;
                    
                case STRING:
                    pack.set(fields[fieldtype], buffer.position());
                    buffer.position(buffer.position() + 4 + buffer.getInt());                    
                    break;
                
                case BYTEARRAY:
                    pack.set(fields[fieldtype], buffer.position());
                    buffer.position(buffer.position() + 4 + buffer.getInt());                    
                    break;
                
                case LONG:
                    pack.set(fields[fieldtype], buffer.position());
                    buffer.position(buffer.position() + 8);
                    break;
                
                case BYTE:
                    pack.set(fields[fieldtype], buffer.position());
                    buffer.position(buffer.position() + 1);
                    break;
                
                case UUID:
                    pack.set(fields[fieldtype], buffer.position());
                    buffer.position(buffer.position() + 16);
                    break;
            }
        }
        
        buffer.rewind();
        
        return pack;
    }

    @Override
    public ByteBuffer serialize(ReplyPackage reply) throws IOException { 
        ByteBuffer buffer = reply.getBuffer();        
        buffer.flip();        
        return buffer;
    }
   
}
