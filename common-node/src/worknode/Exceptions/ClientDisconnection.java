/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package worknode.Exceptions;

/**
 *
 * @author d.kuzminskiy
 */
public class ClientDisconnection extends Exception{

    public ClientDisconnection() {
    }
    
    public ClientDisconnection(String str) {
        super(str);
    }
    
}
