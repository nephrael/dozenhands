/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package worknode.AbstractBusinessLogic;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 *
 * @author d.kuzminskiy
 */
public class StaticGetters {
    
    static public interface Getter<T>
    {
        public T get(ByteBuffer buffer, int pos);
    }
    
    private static final Map<Class<?>, Getter<?>> deres = new HashMap<>();    
    static public <T> void addGetter(Class<T> type, Getter<T> getter)
    {
        deres.put(type, getter);      
    }
    
    
    static
    {
        addDefaultGetters();
    }

    static <T> T Get(Class<T> type, ByteBuffer buffer, Integer pos) {
        
        Getter<T> getter = (Getter<T>) deres.get(type);
        if (getter == null)
            return null;
        
        return getter.get(buffer, pos);
    }
    
     private static void addDefaultGetters() {
        addGetter(Integer.class, (ByteBuffer buffer, int pos) -> buffer.getInt(pos));
        
        addGetter(Long.class, (ByteBuffer buffer, int pos) -> buffer.getLong(pos));
        
        addGetter(UUID.class, (ByteBuffer buffer, int pos) -> new UUID(buffer.getLong(pos), buffer.getLong(pos+8)));
        
        addGetter(Byte.class, (ByteBuffer buffer, int pos) -> buffer.get(pos));
        
        addGetter(byte[].class, (ByteBuffer buffer, int pos) -> {
            buffer.mark();
            buffer.position(pos);
            
            byte[] data = new byte[buffer.getInt()];
            buffer.get(data);
            
            buffer.reset();
            return data;
        });
        
        addGetter(ByteBuffer.class, (ByteBuffer buffer, int pos) -> {
            buffer.mark();
            buffer.position(pos);
            
            int size = buffer.getInt();
            ByteBuffer data = ByteBuffer.allocateDirect(size);
            data.put(buffer);
            
            buffer.reset();
            return data;
        });
        
        addGetter(String.class, (ByteBuffer buffer, int pos) -> {
            int size = buffer.getInt(pos);
            String res = new String();
            
            for (int i=pos+4; i<size+4; ++i) {
                res += buffer.get(i);
            }
            
            return res;
        });
    }
}
