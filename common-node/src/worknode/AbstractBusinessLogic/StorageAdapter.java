/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package worknode.AbstractBusinessLogic;

import dozen.EncryptionData;
import java.nio.channels.FileChannel;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author d.kuzminskiy
 */
public interface StorageAdapter {
    
    public boolean put(EncryptionData data);
    public EncryptionData get(UUID id);

    public void dump(FileChannel file);
}
