/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package worknode.AbstractBusinessLogic;

import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;

/**
 *
 * @author d.kuzminskiy
 */
public class ReplyPackage {
    public enum Field {OBJECT_ID, OBJECT, ERROR, ERROR_DESC};
    
    ByteBuffer buffer = ByteBuffer.allocateDirect(30);

    public ReplyPackage() {
        buffer.putInt(0);
    }
    
    public <T> void set(Field field, T obj)
    {
        try {
            StaticPutter.Put(field.ordinal(), buffer);
            StaticPutter.Put(obj, buffer);
        } catch (BufferOverflowException e) {
            ByteBuffer oldbuffer = buffer;
            buffer = ByteBuffer.allocateDirect(buffer.capacity() + 1024);
            oldbuffer.flip();
            buffer.put(oldbuffer);            
            set(field, obj);
        }
        
        buffer.putInt(0, buffer.position()-4);
    }

    public ByteBuffer getBuffer() {
        return buffer;
    }
    
}
