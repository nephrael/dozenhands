/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package worknode.AbstractBusinessLogic;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author d.kuzminskiy
 */
public class Request {
    
    public static class ObjectNotFound extends Exception {

        public ObjectNotFound(String string) {
            super(string);
        }
    }
    
    public enum Field {REQUEST, OBJECT, OBJECT_ID};
    
    Map<Field, Integer> request = new HashMap<>();
    
    
    final ByteBuffer buffer;
    public Request(ByteBuffer buffer) {
        this.buffer = buffer;
        
    }

    public void set(Field field, Integer pos)
    {
        request.put(field, pos);
    }
    
    public <T> T get(Class<T> type, Field field) throws ObjectNotFound
    {
        Integer pos = request.get(field);
        
        if (pos == null)
            throw new ObjectNotFound("Object " + field.name() + " not found");
        
        T obj = StaticGetters.Get(type, buffer, pos);
        
        if (obj == null)
            throw new ObjectNotFound("Object " + field.name() + " invalid");
        
        return obj;
    }

    
}
