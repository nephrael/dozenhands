/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package worknode.AbstractBusinessLogic;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 *
 * @author d.kuzminskiy
 */
public class StaticPutter {
    
    static public interface Putter<T>
    {
        public boolean put(T obj, ByteBuffer buffer);
    }
    
    private static final Map<Class<?>, Putter<?>> deres = new HashMap<>();   
    
    static public <T> void addPutter(Class<T> type, Putter<T> getter)
    {
        deres.put(type, getter);      
    }
    
    
    static
    {
        AddDefaultPutter();
    }

    static <T> boolean Put(T object,  ByteBuffer buffer) {
        
        Putter<T> putter = (Putter<T>) deres.get(object.getClass());
        if (putter == null)
            return false;
        
        return putter.put(object, buffer);
    }
    
     private static void AddDefaultPutter() {
        addPutter(Integer.class, (Integer obj, ByteBuffer buffer) -> {
            buffer.putInt(obj);
            return true;
        });
        
        addPutter(Long.class, (Long obj, ByteBuffer buffer) -> {
            buffer.putLong(obj);
            return true;
        });
        
        addPutter(UUID.class, (UUID obj, ByteBuffer buffer) -> {
            buffer.putLong(obj.getMostSignificantBits());
            buffer.putLong(obj.getLeastSignificantBits());
            return true;
        });
        
        addPutter(Byte.class, (Byte obj, ByteBuffer buffer) -> {
            buffer.put(obj);
            return true;
        });
        
        addPutter(byte[].class, (byte[] obj, ByteBuffer buffer) -> {
            buffer.putInt(obj.length);
            buffer.put(obj);
            return true;
        });
        
        addPutter(String.class, (String obj, ByteBuffer buffer) -> {
            buffer.putInt(obj.length());
            buffer.put(obj.getBytes());
            return true;
        });
    }
}
