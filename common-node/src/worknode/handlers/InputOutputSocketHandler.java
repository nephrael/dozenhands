/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package worknode.handlers;

import worknode.Exceptions.ClientDisconnection;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.Channels;
import java.nio.channels.CompletionHandler;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import worknode.TcpServer.TcpServer;

/**
 *
 * @author d.kuzminskiy
 */
public class InputOutputSocketHandler implements Runnable {

    final AsynchronousSocketChannel channel;
    final TcpServer server;

    public InputOutputSocketHandler(AsynchronousSocketChannel channel, TcpServer server) throws IOException {
        this.server = server;
        this.channel = channel;
    }

    @Override
    public void run() {
        try {
            ByteBuffer sizebuffer = ByteBuffer.allocateDirect(4);
            while (channel.isOpen()) {

                while (sizebuffer.hasRemaining()) {
                    if (channel.read(sizebuffer).get() == -1) 
                        throw new ClientDisconnection();           
                }

                sizebuffer.rewind();

                int size = sizebuffer.getInt();
                sizebuffer.clear();
                
                try {
                    ByteBuffer input = ByteBuffer.allocateDirect(size);

                    while (input.hasRemaining()) {
                        if (channel.read(input).get() == -1) 
                            throw new ClientDisconnection();
                    }

                    server.addDataReady(this, input);
                } catch (OutOfMemoryError ome){
                    Logger.getLogger(InputOutputSocketHandler.class.getName()).log(Level.WARNING, "Out of memory: {0}", size);
                    Channels.newInputStream(channel).skip(size);
                }
            }
        } catch (ClientDisconnection ex) {
            //Logger.getLogger(InputOutputSocketHandler.class.getName()).log(Level.INFO, "Client disconnected");
        } catch (IOException | InterruptedException | ExecutionException ex) {
            Logger.getLogger(InputOutputSocketHandler.class.getName()).log(Level.SEVERE, null, ex);    
        }
    }
    
    public synchronized void socketWrite(ByteBuffer buffer)
    {
        try {
            channel.write(buffer).get();
        } catch (InterruptedException ex) {
            Logger.getLogger(InputOutputSocketHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ExecutionException ex) {
            Logger.getLogger(InputOutputSocketHandler.class.getName()).log(Level.SEVERE, ex.getMessage());
        }
    }
}
