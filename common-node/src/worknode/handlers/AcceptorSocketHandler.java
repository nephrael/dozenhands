/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package worknode.handlers;

import java.io.IOException;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import worknode.TcpServer.TcpServer;
import static worknode.TcpServer.TcpServer.poolForIO;

/**
 *
 * @author d.kuzminskiy
 */
public class AcceptorSocketHandler implements CompletionHandler<AsynchronousSocketChannel, TcpServer> {

    @Override
    public void completed(AsynchronousSocketChannel result, TcpServer server) {
        try {
            server.accept(this);
            poolForIO.execute(new InputOutputSocketHandler(result, server));
        } catch (IOException ex) {
            Logger.getLogger(TcpServer.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
        }
    }

    @Override
    public void failed(Throwable exc, TcpServer attachment) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
