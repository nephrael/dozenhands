/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package worknode.handlers;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import worknode.AbstractBusinessLogic.AbstractBusinessLogic;
import worknode.AbstractBusinessLogic.ReplyPackage;
import worknode.AbstractBusinessLogic.Request;
import worknode.ProtocolsHandler.AbstractProtocol;

/**
 *
 * @author mail_000
 */
public class DataReadyHandler implements Runnable {

    final ByteBuffer input;
    final AbstractBusinessLogic logic;
    final AbstractProtocol protocol;
    InputOutputSocketHandler iohandler;

    public DataReadyHandler(InputOutputSocketHandler iohandler, ByteBuffer input, AbstractBusinessLogic logic, AbstractProtocol protocol) {
        this.input = input;
        this.logic = logic;
        this.protocol = protocol;
        this.iohandler = iohandler;
    }

    @Override
    public void run() {
        try {
            if (protocol != null) {
                Request request = protocol.deserialize(input);
                ReplyPackage reply = logic.handling(request);
                ByteBuffer serialized = protocol.serialize(reply);
                
                iohandler.socketWrite(serialized);
            }
            else
            {
                Logger.getLogger(InputOutputSocketHandler.class.getName()).log(Level.SEVERE, "Wrong protocol");
            }
        } catch (AbstractProtocol.ParsingException ex) {
            Logger.getLogger(InputOutputSocketHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataReadyHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
