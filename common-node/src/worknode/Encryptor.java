/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package worknode;

import dozen.Dozen;
import dozen.EncryptionData;
import dozen.cpu.DozenParallelPlain;
import dozen.cpu.DozenSuccessivePlain;
import dozen.padding.PKCS7;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import worknode.AbstractBusinessLogic.StorageAdapter;

/**
 *
 * @author d.kuzminskiy
 */
public class Encryptor{
    StorageAdapter storage;

    
    static class UnEncrypted {
        final UUID id;
        final ByteBuffer data;

        public UnEncrypted(UUID id, ByteBuffer data) {
            this.id = id;
            this.data = data;
        }

        public UUID getId() {
            return id;
        }

        public ByteBuffer getData() {
            return data;
        }
    }
    
    final BlockingQueue<UnEncrypted > unEncrypted = new LinkedBlockingQueue<>();
    final BlockingQueue<UnEncrypted > unEncryptedForBatch = new LinkedBlockingQueue<>();
    
    public Encryptor(StorageAdapter storage) {
        this.storage = storage;
        scheduler.setPriority(Thread.MIN_PRIORITY);
        //scheduler.start();
    }
    
    public UUID put(ByteBuffer data) throws IllegalStateException
    {        
        UUID id = UUID.randomUUID();
        //unEncrypted.add(new UnEncrypted(id, data));
        
        return id;
    }
    
    public byte[] get(UUID id)
    {
        EncryptionData uncrypted = storage.get(id);
        
        Dozen cypher = new DozenParallelPlain(uncrypted.getKey(), uncrypted.getCounter());
        
        ByteBuffer buffer = uncrypted.getData();
        
        buffer.rewind();
        byte[] data = new byte[buffer.remaining()];
        buffer.get(data);
        
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        ByteArrayOutputStream out = new ByteArrayOutputStream(data.length);
        if (cypher.decrypt(in, out))
            return out.toByteArray();
        
        return null;
    }

    UUID putUnEnecrypted(ByteBuffer array) {
        UUID id = UUID.randomUUID();
        //unEncryptedForBatch.add(new UnEncrypted(id, array));
        
        return id;
    }
        
    UUID totalCrypt() {
        UUID id = UUID.randomUUID();
        
        List<UnEncrypted> uncrypted = new LinkedList<>();
        unEncryptedForBatch.drainTo(uncrypted);
        
        int size = 0;
        for (UnEncrypted encryptionData : uncrypted) {
            size += encryptionData.getData().capacity();
        }
        
        ByteBuffer total = ByteBuffer.allocateDirect(size);
        for (UnEncrypted encryptionData : uncrypted) {
            ByteBuffer buffer = encryptionData.getData();
            buffer.rewind();
            total.put(buffer);
        }
        
        EncryptionData res = encrypt(new EncryptionData(total, null, 0, id));
        
        if (res != null)
            storage.put(res);
        
        return id;
    }
    
    static private EncryptionData encrypt(EncryptionData uncrypted) {
        Random random = new Random();

        byte[] key = new byte[dozen.Dozen.KEY_SIZE];
        random.nextBytes(key);

        uncrypted.setKey(key);
        uncrypted.setCounter(random.nextLong());

        Dozen cypher = new DozenSuccessivePlain(uncrypted.getKey(), uncrypted.getCounter());
        cypher.setPadding(new PKCS7());

        ByteBuffer buffer = uncrypted.getData();
        buffer.rewind();
        byte[] dt = new byte[buffer.remaining()];
        buffer.get(dt);

        ByteArrayInputStream in = new ByteArrayInputStream(dt);
        ByteArrayOutputStream out = new ByteArrayOutputStream(dt.length + 64);
        if (cypher.encrypt(in, out))
        {
            uncrypted.setData(ByteBuffer.wrap(out.toByteArray()));
            return uncrypted;
        }

        return null;
    }
    
    void dumpAll(FileChannel file) {
        storage.dump(file);
    }
    
    final Thread scheduler = new Thread("Encryptor")
    {
        @Override
        public void run() {
            while(!isInterrupted())
            {
                try {
                    UnEncrypted data = unEncrypted.take();
                    
                    EncryptionData uncrypted = new EncryptionData(data.getData(), null, 0, data.getId());
                    EncryptionData encrypted = encrypt(uncrypted);
                    if (encrypted != null)
                        storage.put(encrypted);
                    
                } catch (InterruptedException ex) {
                    Logger.getLogger(Encryptor.class.getName()).log(Level.SEVERE, null, ex);
                }
            } 
        }
    };

}
