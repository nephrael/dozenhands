/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package worknode.util;

import java.io.ByteArrayOutputStream;

/**
 *
 * @author d.kuzminskiy
 */
public class NonCopyByteArrayOutputStream extends ByteArrayOutputStream{

    public NonCopyByteArrayOutputStream(int size) 
    {
        super(size);
    }

    public NonCopyByteArrayOutputStream() {
        super();
    }
    
    @Override
    public byte[] toByteArray() {
        return buf;
    }
    
}
