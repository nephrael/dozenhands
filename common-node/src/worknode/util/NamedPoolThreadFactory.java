/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package worknode.util;

import java.util.concurrent.ThreadFactory;

/**
 *
 * @author d.kuzminskiy
 */
public class NamedPoolThreadFactory implements ThreadFactory{
    final String prefix;
    int poolNumber = 1;
    int priority;
    
    public NamedPoolThreadFactory(String prefix)
    {
        this(prefix, Thread.NORM_PRIORITY);
    }

    public NamedPoolThreadFactory(String prefix, int priority) {
        this.prefix = prefix;
        this.priority = priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
    
    @Override
    public Thread newThread(Runnable r) {
        Thread t = new Thread(r, prefix + " " + poolNumber++);
        t.setPriority(priority);
        return t;
    }
    
    
    
}
