/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package worknode.util;

import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author mail_000
 */
public class CachedLimitedExecutor extends ThreadPoolExecutor {
    
    public CachedLimitedExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, ThreadFactory threadFactory) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, new SynchronousQueue<>(), threadFactory, new CallerRunsPolicy());
    }
    
    public CachedLimitedExecutor(int corePoolSize, int maximumPoolSize) {
        this(corePoolSize, maximumPoolSize, Executors.defaultThreadFactory());
    }
    
    public CachedLimitedExecutor(int maximumPoolSize) {
        this(0, maximumPoolSize, Executors.defaultThreadFactory());
    }
    
    public CachedLimitedExecutor(int corePoolSize, int maximumPoolSize, ThreadFactory threadFactory) {
        this(corePoolSize, maximumPoolSize, 15L, TimeUnit.SECONDS, threadFactory);
    }

    @Override
    public void execute(Runnable command) {
        super.execute(command); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}