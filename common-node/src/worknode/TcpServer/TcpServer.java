/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package worknode.TcpServer;

import worknode.util.CachedLimitedExecutor;
import worknode.handlers.AcceptorSocketHandler;
import worknode.handlers.DataReadyHandler;
import worknode.util.NamedPoolThreadFactory;
import worknode.ProtocolsHandler.AbstractProtocol;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousChannelGroup;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.logging.Level;
import java.util.logging.Logger;
import worknode.AbstractBusinessLogic.AbstractBusinessLogic;
import worknode.handlers.InputOutputSocketHandler;

/**
 *
 * @author d.kuzminskiy
 */
public class TcpServer {

    public static CachedLimitedExecutor poolForAccept = new CachedLimitedExecutor(1, 100, new NamedPoolThreadFactory("Accepter"));
    public static CachedLimitedExecutor poolForIO = new CachedLimitedExecutor(1, 1000, new NamedPoolThreadFactory("IO"));    
    public static CachedLimitedExecutor poolForReadyData = new CachedLimitedExecutor(1, 100, new NamedPoolThreadFactory("Executor"));
    
    final AsynchronousServerSocketChannel serverSocketChannel = AsynchronousServerSocketChannel.open(AsynchronousChannelGroup.withCachedThreadPool(poolForAccept, 1));
    final InetAddress address;
    final int port;    

    final AbstractProtocol protocol;
    final AbstractBusinessLogic logic;
    
    public TcpServer(InetAddress address, int port, AbstractProtocol protocol, AbstractBusinessLogic logic) throws IOException {       
        this.address = address;
        this.port = port;
        this.protocol = protocol;
        this.logic = logic;
    }

    public void start() {
        try {
            serverSocketChannel.bind(new InetSocketAddress(address, port));
            accept(new AcceptorSocketHandler());           
            
        } catch (IOException ex) {
            Logger.getLogger(TcpServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addDataReady(InputOutputSocketHandler iohandler, ByteBuffer input) {
        poolForReadyData.execute(new DataReadyHandler(iohandler, input, logic, protocol));
    }

    public void accept(AcceptorSocketHandler handler) {
        serverSocketChannel.accept(this, handler);
    }

}
