/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package worknode;

import dozen.EncryptionData;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import worknode.AbstractBusinessLogic.StorageAdapter;

/**
 *
 * @author d.kuzminskiy
 */
public class Storage implements StorageAdapter{

    final ConcurrentHashMap<UUID, EncryptionData> store = new ConcurrentHashMap<>(1000, 0.1f);
    final ConcurrentLinkedQueue<EncryptionData> uncrypted = new ConcurrentLinkedQueue<>();
    
    @Override
    public boolean put(EncryptionData data) {
        return store.put(data.getId(), data) != null;
    }

    @Override
    public EncryptionData get(UUID id) {
        return store.get(id);
    }

    @Override
    public synchronized void dump(FileChannel file) {
        try {
            Map <UUID, ByteBuffer> map =  new HashMap<>();
            int size = 0;
            synchronized (store)
            {
                for (Map.Entry<UUID, EncryptionData> encryptionData : store.entrySet()) {
                    EncryptionData data = encryptionData.getValue();
                    
                    map.put(encryptionData.getKey(), data.getData());
                    size += data.getData().capacity() + 20;
                    data.setData(null);
                }
            }
            
            ByteBuffer wrBuf = file.map(FileChannel.MapMode.READ_WRITE, 0, size);
            for (Map.Entry<UUID, ByteBuffer> encryptionData : map.entrySet()) {
                UUID key = encryptionData.getKey();
                ByteBuffer value = encryptionData.getValue();
                
                wrBuf.putLong(key.getMostSignificantBits());
                wrBuf.putLong(key.getLeastSignificantBits());
                wrBuf.putInt(value.capacity());
                
                value.rewind();
                wrBuf.put(value);
            }
        } catch (IOException ex) {
            Logger.getLogger(Storage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
