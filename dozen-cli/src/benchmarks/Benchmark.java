/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package benchmarks;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author d.kuzminskiy
 */
public class Benchmark {

    /**
     * @param args the command line arguments
     */
    
    static byte[] getKey()
    {
        FileInputStream fs = null;
        try {
            byte[] key = new byte[256];
            fs = new FileInputStream("C:\\1\\key.bin");
            fs.read(key);
            return key;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Benchmark.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Benchmark.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fs.close();
            } catch (IOException ex) {
                Logger.getLogger(Benchmark.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
    
    static byte[] getArray(int size)
    {
        FileInputStream fs = null;
        try {
            byte[] key = new byte[size];
            fs = new FileInputStream("C:\\1\\input." + size + ".bin");
            fs.read(key, 0, size);
            return key;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Benchmark.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Benchmark.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fs.close();
            } catch (IOException ex) {
                Logger.getLogger(Benchmark.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
    
    static byte[] getGood(int size)
    {
        FileInputStream fs = null;
        try {
            fs = new FileInputStream("C:\\1\\good." + size + ".bin");
            byte[] key = new byte[fs.available()];
            fs.read(key);
            return key;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Benchmark.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Benchmark.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fs.close();
            } catch (IOException ex) {
                Logger.getLogger(Benchmark.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
    
    static boolean isSame(byte[] array, ByteArrayOutputStream stream)
    {
        return Arrays.equals(array, stream.toByteArray());
    }
    
    static void check(int size)
    {
        byte[] key = getKey();
        byte[] input = getArray(size);
        byte[] good = getGood(size);
        ByteArrayInputStream in = new ByteArrayInputStream(input);
        
        System.out.println("Check: " + size);
        
        {
            ByteArrayOutputStream out1 = new ByteArrayOutputStream(input.length);

            dozen.cpu.DozenSuccessiveComplex al1 = new dozen.cpu.DozenSuccessiveComplex(key);
            al1.encrypt(in, out1);
            
            if (isSame(good, out1))
                System.out.print("DozenSuccessive: ok\n\n");
            else
                System.out.print("DozenSuccessive: fail\n\n");
        }
        
        {
            in.reset();
            ByteArrayOutputStream out2 = new ByteArrayOutputStream(input.length);

            dozen.cpu.DozenSuccessivePlain al2 = new dozen.cpu.DozenSuccessivePlain(key);
            al2.encrypt(in, out2);
            
            if (isSame(good, out2))
                System.out.print("DozenSuccessivePlain: ok\n\n");
            else
                System.out.print("DozenSuccessivePlain: fail\n\n");
        }
        
        {
            in.reset();
            ByteArrayOutputStream out3 = new ByteArrayOutputStream(input.length);

            dozen.cpu.DozenParallelComplex al3 = new dozen.cpu.DozenParallelComplex(key);
            al3.encrypt(in, out3);
            
            if (isSame(good, out3))
                System.out.print("DozenParallel: ok\n\n");
            else
                System.out.print("DozenParallel: fail\n\n");
        }
        
        {
            in.reset();
            ByteArrayOutputStream out4 = new ByteArrayOutputStream(input.length);

            dozen.cpu.DozenParallelPlain al4 = new dozen.cpu.DozenParallelPlain(key);
            al4.encrypt(in, out4);
            
            if (isSame(good, out4))
                System.out.print("DozenParallelPlain: ok\n\n");
            else
                System.out.print("DozenParallelPlain: fail\n\n");
        }
        
        System.out.println("###########################################");
    }
    
    public static void main(String[] args) {
        check(1000);        
        check(10000);   
        check(100000);   
        check(1000000);   
    }
    
}
