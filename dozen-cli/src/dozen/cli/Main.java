/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dozen.cli;

import dozen.Cypher;
import dozen.Dozen;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 *
 * @author d.kuzminskiy
 */
public class Main {
    /// Attributes
    private static final String copy_right = "Copyright © 2014. Dmitry Kuzminskiy (mail@nephrael.ru). All Rights Reserved.";
    CommandLine m_options;
    
    public class WorkingStreams
    {
        public InputStream input = null;
        public InputStream key = null;
        public OutputStream output = null;        
    }
    
    /// Methods
    private Main(CommandLine options) {
        m_options = options;
    }
    
    private static void showHelp(Options options) {
        PrintWriter writer = new PrintWriter(System.out);
        HelpFormatter helpFormatter = new HelpFormatter();
        helpFormatter.printHelp(writer, 120,"java dozen.jar", "Options", options, 3, 7, "\n"+copy_right, true);
        writer.flush();
    }
    
    private static Options setOptions() {
        Options options = new Options();
        options.addOption(new Option( "help", "print this message" ));
        options.addOption(new Option( "gpu", "compute on gpu" ));
        options.addOption(new Option( "cpu", "compute on cpu" ));
        
        // Add input group
        {
            OptionGroup og_input = new OptionGroup();

            Option input_file = new Option("if", true, "use given file for input");
            input_file.setArgName("filename");
            og_input.addOption(input_file);

            og_input.setRequired(true);
            options.addOptionGroup(og_input);
        }
        
        // Add key group
        {
            OptionGroup og_key = new OptionGroup();

            Option key_file = new Option("kf", true, "use given file for key");
            key_file.setArgName("filename");
            og_key.addOption(key_file);
            
            Option key_counter = new Option("kc", true, "use [0x00 .. 0xff] bytes for key");
            key_counter.setArgs(0);
            og_key.addOption(key_counter);

            og_key.setRequired(true);
            options.addOptionGroup(og_key);
        }
        
        // Add key group
        {
            OptionGroup og_out = new OptionGroup();
            
            Option out_file = new Option("of", true, "use given file for output");
            out_file.setArgName("filename");
            og_out.addOption(out_file);
            
            Option out_console = new Option("oc", true, "output to console");
            out_console.setArgs(0);
            og_out.addOption(out_console);
            
            og_out.setRequired(true);
            options.addOptionGroup(og_out);
        }  
        
        // Add key group
        {
            OptionGroup og_type = new OptionGroup();
            
            Option type_suc = new Option("sc", true, "encode successively complex");
            type_suc.setArgs(0);
            og_type.addOption(type_suc);
            
            Option type_sucp = new Option("sp", true, "encode successively plain");
            type_sucp.setArgs(0);
            og_type.addOption(type_sucp);
            
            Option type_parallel = new Option("pc", true, "encode parallely complex [default]");
            type_parallel.setArgs(0);
            og_type.addOption(type_parallel);
            
            Option type_parallelp = new Option("pp", true, "encode parallely plain");
            type_parallelp.setArgs(0);
            og_type.addOption(type_parallelp);
            
            og_type.setRequired(false);
            options.addOptionGroup(og_type);
        } 
        return options;
    }

    private static CommandLine parse(String[] args, Options options) throws ParseException
    {
        CommandLineParser parser = new BasicParser();
        return parser.parse(options, args);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Options options = setOptions();        
        try {
            CommandLine cmd = parse(args, options);

            if (cmd.hasOption("help"))
                showHelp(options);
            
            new Main(cmd).execute();
            
        } catch (ParseException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Parse argument: " + ex.getLocalizedMessage());
            showHelp(options);
        }
    }

    private WorkingStreams open() {
        CommandLine cmd = m_options;
        WorkingStreams streams = new WorkingStreams();
        
        if (cmd.hasOption("if"))
        {
            String ifile = cmd.getOptionValue("if");
            try {
                streams.input = new FileInputStream(ifile);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                System.out.printf("File %s not found\n", ifile);
                System.exit(101);
            }
        }
        else
        {
            System.out.printf("File not set\n");
            System.exit(102);
        }
        
        if (cmd.hasOption("kf"))
        {
            String ifile = cmd.getOptionValue("kf");
            try {
                streams.key = new FileInputStream(ifile);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                System.out.printf("File %s not found\n", ifile);
                System.exit(101);
            }
        } 
        else if (cmd.hasOption("kc"))
        {
            byte[] arr = new byte[256];
            for (int i=0; i<255; ++i)
                arr[i] = (byte) i;
            
            streams.key = new ByteArrayInputStream(arr);
        }
        else 
        {
            System.out.printf("Key not set\n");
            System.exit(103);
        }
        
        if (cmd.hasOption("of"))
        {
            String ifile = cmd.getOptionValue("of");
            try {
                streams.output = new FileOutputStream(ifile);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                System.out.printf("File %s cannot be open\n", ifile);
                System.exit(104);
            }
        }
        else if (cmd.hasOption("oc"))
        {
            streams.output = System.out;
        }
        else
        {
            System.out.printf("Output file not set\n");
            System.exit(103);
        }
        
        return streams;   
    }
    
    private void execute(){
        boolean isGpu = m_options.hasOption("gpu");
        boolean isCpu = m_options.hasOption("cpu");
        
        if (!isGpu && !isCpu)
            isGpu = isCpu = true;    
        
        if (isCpu)
        {
            WorkingStreams streams = open();
            byte[] key = new byte[Dozen.KEY_SIZE];
            try {
                if (streams.key.read(key,0,Dozen.KEY_SIZE) == Dozen.KEY_SIZE)
                {
                    Cypher enc;
                    
                    if (m_options.hasOption("sp"))
                        enc = new dozen.cpu.DozenSuccessivePlain(key, 500);
                    else if (m_options.hasOption("sc"))
                        enc = new dozen.cpu.DozenSuccessiveComplex(key, 500);
                    else if (m_options.hasOption("pc"))
                        enc = new dozen.cpu.DozenParallelComplex(key, 500);
                    else if (m_options.hasOption("gpu"))
                        enc = new dozen.gpu.DozenGpuPlain(key, 500);
                    else 
                        enc = new dozen.cpu.DozenParallelPlain(key, 500);
                    
                    enc.encrypt(streams.input, streams.output);
                }
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if (isGpu)
        {
            WorkingStreams streams = open();
            byte[] key = new byte[Dozen.KEY_SIZE];
            try {
                if (streams.key.read(key,0,Dozen.KEY_SIZE) == Dozen.KEY_SIZE)
                {
                    Cypher enc = new dozen.gpu.DozenGpuPlain(key, 500);                    
                    enc.encrypt(streams.input, streams.output);
                }
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
