﻿#pragma once

#include <memory>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ip/address.hpp>
#include <boost/thread/thread.hpp>

#include <BusinessLogic/AbstractBusinessLogic.hpp>

class TcpServer: public boost::asio::io_service
{
    boost::asio::ip::tcp::acceptor _acceptor;
    std::shared_ptr<AbstractBusinessLogic> _logic;

    boost::thread_group _ioPool;

public:
    TcpServer(  std::shared_ptr<AbstractBusinessLogic> logic,
                unsigned short port,
                const boost::asio::ip::address& addr = boost::asio::ip::address());


    std::size_t run();
    void        accept();

};
