﻿#pragma once

class AbstractBusinessLogic;

class RequestPackage
{
    friend class boost::serialization::access;

    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {

    }
public:

    virtual void handle(AbstractBusinessLogic& handler) const = 0;
};

class EncodeRequest: public RequestPackage
{
    friend class boost::serialization::access;

    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {

    }
public:

    virtual void handle(AbstractBusinessLogic& logic) const;
    /*{
        logic.handling(*this);
    }*/
};
