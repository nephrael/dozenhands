﻿#pragma once

#include <ostream>
#include <istream>

class BoostSerProtocol {

public:
    template<typename ArOut_, typename T>
    bool serialize( const T &reply, std::ostream &stream) const
    {
        ArOut_ oa(stream);
        oa << reply;
        return true;
    }

    template< typename ArIn_, typename T>
    bool deserialize(T& obj, std::istream &stream) const
    {
        ArIn_ ia(stream);
        ia >> obj;
        return true;
    }
};
