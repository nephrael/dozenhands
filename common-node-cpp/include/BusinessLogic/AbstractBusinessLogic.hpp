﻿#pragma once

#include <memory>
#include <Protocol/ReplyPackage.hpp>
#include <Protocol/RequestPackage.hpp>


class AbstractBusinessLogic
{
public:
     virtual std::shared_ptr<RequestPackage> handling(EncodeRequest& reply) = 0;
};
