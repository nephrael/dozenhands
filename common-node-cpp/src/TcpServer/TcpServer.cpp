﻿#include <TcpServer/TcpServer.hpp>

TcpServer::TcpServer(   std::shared_ptr<AbstractBusinessLogic> logic,
                        unsigned short port,
                        const boost::asio::ip::address& addr)
    : _acceptor(*this),  _logic(logic)
{
    boost::asio::ip::tcp::endpoint ep(addr, port);
    _acceptor.open(ep.protocol());
    _acceptor.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
    _acceptor.bind(ep);
}

std::size_t TcpServer::run()
{
    _acceptor.listen();
    accept();
    return boost::asio::io_service::run();
}

void TcpServer::accept()
{
    std::shared_ptr<boost::asio::ip::tcp::socket> socket;

    _acceptor.async_accept(*socket,
       [this, socket](const boost::system::error_code& error)
       {
         //if (!error)
         //   connection->start();

         this->accept();
       }
    );
}
