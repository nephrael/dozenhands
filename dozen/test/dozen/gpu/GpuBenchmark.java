package dozen.gpu;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import dozen.cpu.DozenParallelPlain;
import dozen.cpu.DozenPlain;
import dozen.cpu.DozenSuccessivePlain;
import java.io.ByteArrayInputStream;
import java.util.Random;

/**
 *
 * @author d.kuzminskiy
 */
public class GpuBenchmark {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws DozenGpuPlain.NoDeviceExeption {
       
        cryptSize(1024*1024*512);
        cryptSize(1024*1024*128);
        cryptSize(1024*1024);
    }
    
    public static void cryptSize(int size) throws DozenGpuPlain.NoDeviceExeption
    {
        byte[] buffer = new byte[size];
        
        byte[] key = new byte[256];
        Random random = new Random();
        random.nextBytes(key);
        
        crypt(new DozenGpuPlain(key, 0), buffer, key);
        crypt(new DozenParallelPlain(key, 0), buffer, key);
        crypt(new DozenSuccessivePlain(key, 0), buffer, key);
    }
    
    public static void crypt(DozenPlain encrypter, byte[] buffer, byte[] key) {
      
        System.out.println("Crypt with: " + encrypter.getClass().getName());
        
        double total = 0;
        for (int i=0; i<1; ++i)
        {
            ByteArrayInputStream bais = new ByteArrayInputStream(buffer);
            if (!encrypter.encrypt(bais, null))
                System.out.println("Error");
            
            System.out.println(Double.toString(encrypter.lastTraceTime));
            total += encrypter.lastTraceTime;
        }
        
        System.out.println("Total = " + total);
    }
    
}
