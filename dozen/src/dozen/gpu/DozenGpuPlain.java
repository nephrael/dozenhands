/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dozen.gpu;

import dozen.cpu.DozenPlain;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import jcuda.Pointer;

import jcuda.driver.*;
import jcuda.runtime.JCuda;

public class DozenGpuPlain extends DozenPlain {

    public static class NoDeviceExeption extends Exception {

    }

    static CUmodule module = null;
    static CUdevice device = null;
    private CUfunction function;

    public DozenGpuPlain(byte[] key) throws NoDeviceExeption {
        super(key);
        init();
    }

    public DozenGpuPlain(byte[] key, byte[] iv) throws NoDeviceExeption {
        super(key, iv);
        init();
    }

    public DozenGpuPlain(byte[] key, long counter) throws NoDeviceExeption {
        super(key, counter);
        init();
    }

    private void init() throws NoDeviceExeption {

        if (module == null) {

            InputStream stream = getClass().getResourceAsStream("kernel.cubin");
            byte[] file = null;
            try {
                file = new byte[stream.available()];
                stream.read(file);
            } catch (IOException ex) {
                Logger.getLogger(DozenGpuPlain.class.getName()).log(Level.SEVERE, null, ex);
            }

            JCudaDriver.cuInit(0);

            int deviceCount[] = new int[1];
            if (JCudaDriver.cuDeviceGetCount(deviceCount) != CUresult.CUDA_SUCCESS || deviceCount[0] == 0) 
                throw new NoDeviceExeption();            

            device = new CUdevice();
            JCudaDriver.cuDeviceGet(device, deviceCount[0] - 1);
            CUcontext context = new CUcontext();
            JCudaDriver.cuCtxCreate(context, 0, device);

            module = new CUmodule();

            if (JCudaDriver.cuModuleLoadData(module, file) != CUresult.CUDA_SUCCESS)
                throw new NoDeviceExeption();
        }

        function = new CUfunction();

        String moduleName = "DozenKernelEcb";

        switch (getMode()) {
            case CTR:
                moduleName = "DozenKernelCtr";
                break;
            case ECB:
                moduleName = "DozenKernelEcb";
                break;
        }

        JCudaDriver.cuModuleGetFunction(function, module, moduleName);
    }

    boolean encryptOnGpu(byte[] data, long counterOffset) {

        CUdevprop prop = new CUdevprop();
        JCudaDriver.cuDeviceGetProperties(prop, device);

        int threads = (prop.sharedMemPerBlock - 32) / BLOCK_SIZE;

        CUdeviceptr dev_key = new CUdeviceptr();
        JCudaDriver.cuMemAlloc(dev_key, m_key.length);
        if (JCudaDriver.cuMemcpyHtoD(dev_key, Pointer.to(m_key), m_key.length) != CUresult.CUDA_SUCCESS)
            return false;

        int current_offset = 0;
        while (current_offset < data.length) {
            long[] total_space = {0};
            long[] free_space = {0};
            JCudaDriver.cuMemGetInfo(free_space, total_space);

            int available_size = (int) (free_space[0] * 0.9);
            if (current_offset + available_size > data.length) {
                available_size = data.length - current_offset;
            }

            if (available_size > threads * 65535 * BLOCK_SIZE) {
                available_size = threads * 65535 * BLOCK_SIZE;
            } else if (available_size % 64 != 0) {
                available_size += 64 - (available_size % BLOCK_SIZE);
            }

            int available_blocks = available_size / BLOCK_SIZE;

            CUdeviceptr dev_data = new CUdeviceptr();
            if (JCudaDriver.cuMemAlloc(dev_data, available_size) != CUresult.CUDA_SUCCESS)
            {
                JCudaDriver.cuMemFree(dev_key);
                return false;
            }

            if (JCudaDriver.cuMemcpyHtoD(dev_data, Pointer.to(data).withByteOffset(current_offset), available_size) != CUresult.CUDA_SUCCESS)
            {
                JCudaDriver.cuMemFree(dev_key);
                JCudaDriver.cuMemFree(dev_data);
                return false;
            }
            
            Pointer kernelParameters = null;
            switch (getMode()) {
                case CTR:
                    kernelParameters = Pointer.to(
                            Pointer.to(dev_data),
                            Pointer.to(new int[]{available_blocks}),
                            Pointer.to(dev_key),
                            Pointer.to(new long[]{counterOffset})
                    );
                    break;
                case ECB:
                    kernelParameters = Pointer.to(
                            Pointer.to(dev_data),
                            Pointer.to(new int[]{available_blocks}),
                            Pointer.to(dev_key)
                    );
                    break;
            }

            int blocks = (available_blocks / threads) + 1;

            JCudaDriver.cuLaunchKernel(function,
                    blocks, 1, 1, // Grid dimension
                    threads, 1, 1, // Block dimension
                    0, null, // Shared memory size and stream
                    kernelParameters, null // Kernel- and extra parameters
            );

            int res = JCudaDriver.cuCtxSynchronize();
            if (res != CUresult.CUDA_SUCCESS)
            {
                res = JCudaDriver.cuMemFree(dev_key);
                res = JCudaDriver.cuMemFree(dev_data);
                return false;
            }

            if (JCudaDriver.cuMemcpyDtoH(Pointer.to(data), dev_data, available_size)!= CUresult.CUDA_SUCCESS)
            {
                JCudaDriver.cuMemFree(dev_key);
                JCudaDriver.cuMemFree(dev_data);
                return false;
            }
            
            JCudaDriver.cuMemFree(dev_data);
            
            current_offset += available_size;
        }
        
        JCudaDriver.cuMemFree(dev_key);    
        return true;
    }
    
    @Override
        protected boolean encryptECB(byte[] data) {
        return encryptOnGpu(data, 0);
    }

    @Override
        protected boolean encryptCTR(byte[] data, long counterOffset) {
        return encryptOnGpu(data, counterOffset);
    }
}
