/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dozen;

import dozen.padding.PKCS7;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author d.kuzminskiy
 */
public abstract class Cypher {
    
    public double lastTraceTime = 0;
    public enum Mode {ECB, CTR}
    
    protected byte[] m_key;    
    protected final byte[] m_iv;
    protected Padding m_padding = new PKCS7();
    protected long m_counter;
    protected final Mode m_mode;
    
    public Cypher(Mode mode, byte[] key, byte[] iv, long counterStart)
    {
        m_key = key;               
        m_iv = iv;
        m_counter = counterStart;
        m_mode = mode;
    }
    
    public Cypher(byte[] key, long counterStart)
    {
        this(Mode.CTR, key, null, counterStart);
    }
    
    public Cypher(byte[] key, byte[] iv)
    {
        this(Mode.ECB, key, iv, 0);
    }
    
    public void setKey(byte[] key)
    {
        m_key = key;
    }
    
    public byte[] getKey()
    {
        return m_key;
    }
    
    public void setPadding(Padding padding)
    {
        m_padding = padding;
    }
    
    public Mode getMode() {
        return m_mode;        
    }
    
    public void setCounter(long counter) {
        m_counter = counter;
    }
    
    byte[] read(InputStream input, boolean padding)
    {        
        try {
            if (input.available() == 0)
                return null;
            
            int count = input.available();
            int size = m_padding.calculate(count, getBlockSize());
            
            byte[] data = new byte[size];
            
            if (input.read(data, 0, count) == count)
            {
                if (padding)
                    m_padding.pad(data, 0, size, count);
                return data;
            }
            
        } catch (IOException ex) {
            Logger.getLogger(Dozen.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public boolean encrypt(InputStream input, OutputStream output)
    {
        try {
            byte[] array;
            while((array = read(input, true)) != null)
            {
                long start = System.nanoTime();

                boolean ok = false;

                switch(m_mode)
                {
                    case ECB: ok = encryptECB(array); break;                    
                    case CTR: ok = encryptCTR(array, m_counter); break;
                }

                lastTraceTime = System.nanoTime()-start;
                
                if (ok && output != null)
                    output.write(array);

                return ok;        
            }
            return true;   
        } catch (IOException ex) {
            Logger.getLogger(Dozen.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    }
    
    public boolean decrypt(InputStream input, OutputStream output)
    {
        byte[] array;
        while((array = read(input, false)) != null)
        {
            long start = System.nanoTime();
            
            boolean ok = false;
            
            switch(m_mode)
            {
                case ECB: throw new UnsupportedOperationException("ECB decrypt not implemented");
                case CTR: ok = encryptCTR(array, m_counter); break;        
            }
            
            lastTraceTime = System.nanoTime()-start;
            
            if (ok)
            {
                try {
                    int outsize = m_padding.realSize(array, getBlockSize());
                    if (output != null)
                        output.write(array, 0, outsize);
                } catch (IOException ex) {
                    Logger.getLogger(Dozen.class.getName()).log(Level.SEVERE, null, ex);
                    return false;
                }
            }
            
            return ok;
        }
        return true;
    }
    
    /// Abstracts
    protected abstract boolean encryptECB(byte[] data);
    protected abstract boolean encryptCTR(byte[] data, long counterOffset);
    protected abstract int getBlockSize();
}
