/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dozen.cpu;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author d.kuzminskiy
 */
public class ThreadPoolManager {
    
    final int maximumPoolSize = Runtime.getRuntime().availableProcessors();
    ThreadPoolExecutor tpe = new ThreadPoolExecutor(maximumPoolSize, maximumPoolSize, 0, TimeUnit.NANOSECONDS, new LinkedBlockingQueue<Runnable>());
    
    public void addWorker(Runnable r)
    {
        tpe.execute(r);
    }
    
    public void join()
    {
        try {
            tpe.shutdown();
            tpe.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException ex) {
            Logger.getLogger(ThreadPoolManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
