/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dozen.cpu;

import dozen.Dozen;

/**
 *
 * @author d.kuzminskiy
 */
public abstract class DozenPlain extends Dozen {

    public DozenPlain(byte[] key) {
        super(key);
    }

    public DozenPlain(byte[] key, byte[] iv) {
        super(key, iv);
    }
    
    public DozenPlain(byte[] key, long counter) {
        super(key, counter);
    }

    protected final void AddRoundKey(byte[] block, int offset) {
        for (int i = 0; i < BLOCK_SIZE; i++) {
            block[offset + i] ^= m_key[i];
        }
    }

    //
    protected final void AddRoundSubKeyX(byte[] data, int loffset, int lkoffset) {
        for (int i = 0; i < ROW_COUNT; ++i) {
            for (int j = 0; j < COL_COUNT; ++j) {
                data[loffset + j * 4 + (3 - i)] ^= m_key[lkoffset + j * 4 + (3 - i)];
            }
        }
    }

    protected final void AddRoundSubKeyY(byte[] data, int loffset, int lkoffset) {
        for (int i = 0; i < ROW_COUNT; ++i) {
            for (int j = 0; j < COL_COUNT; ++j) {
                data[loffset + j * 16 + (3 - i)] ^= m_key[lkoffset + j * 16 + (3 - i)];
            }
        }
    }

    protected final void AddRoundSubKeyZ(byte[] data, int loffset, int lkoffset) {
        for (int i = 0; i < ROW_COUNT; ++i) {
            for (int j = 0; j < COL_COUNT; ++j) {
                data[loffset + j * 4 + 16 * (3 - i)] ^= m_key[lkoffset + j * 4 + 16 * (3 - i)];
            }
        }
    }

    ///
    protected final void SubBytesX(byte[] data, int loffset) {
        for (int i = 0; i < ROW_COUNT; ++i) {
            for (int j = 0; j < COL_COUNT; ++j) {
                int pos = loffset + j * 4 + (3 - i);
                data[pos] = SBox[data[pos] & 0xFF];
            }
        }
    }

    protected final void SubBytesY(byte[] data, int loffset) {
        for (int i = 0; i < ROW_COUNT; ++i) {
            for (int j = 0; j < COL_COUNT; ++j) {
                int pos = loffset + j * 16 + (3 - i);
                data[pos] = SBox[data[pos] & 0xFF];
            }
        }
    }

    protected final void SubBytesZ(byte[] data, int loffset) {
        for (int i = 0; i < ROW_COUNT; ++i) {
            for (int j = 0; j < COL_COUNT; ++j) {
                int pos = loffset + j * 4 + 16 * (3 - i);
                data[pos] = SBox[data[pos] & 0xFF];
            }
        }
    }

    ///
    protected final void MixRowsX(byte[] data, int loffset) {
        for (int i = 0; i < ROW_COUNT; ++i) {
            byte a0 = data[loffset + (3 - i)];
            byte a1 = data[loffset + 4 + (3 - i)];
            byte a2 = data[loffset + 8 + (3 - i)];
            byte a3 = data[loffset + 12 + (3 - i)];

            byte b0 = (byte) (a0 >> 7 & 0x1B ^ a0 << 1);
            byte b1 = (byte) (a1 >> 7 & 0x1B ^ a1 << 1);
            byte b2 = (byte) (a2 >> 7 & 0x1B ^ a2 << 1);
            byte b3 = (byte) (a3 >> 7 & 0x1B ^ a3 << 1);

            data[loffset + (3 - i)] = (byte) (b0 ^ a3 ^ a2 ^ b1 ^ a1);
            data[loffset + 4 + (3 - i)] = (byte) (b1 ^ a0 ^ a3 ^ b2 ^ a2);
            data[loffset + 8 + (3 - i)] = (byte) (b2 ^ a1 ^ a0 ^ b3 ^ a3);
            data[loffset + 12 + (3 - i)] = (byte) (b3 ^ a2 ^ a1 ^ b0 ^ a0);
        }
    }

    protected final void MixRowsY(byte[] data, int loffset) {
        for (int i = 0; i < ROW_COUNT; ++i) {
            byte a0 = data[loffset + (3 - i)];
            byte a1 = data[loffset + 16 + (3 - i)];
            byte a2 = data[loffset + 32 + (3 - i)];
            byte a3 = data[loffset + 48 + (3 - i)];

            byte b0 = (byte) (a0 >> 7 & 0x1B ^ a0 << 1);
            byte b1 = (byte) (a1 >> 7 & 0x1B ^ a1 << 1);
            byte b2 = (byte) (a2 >> 7 & 0x1B ^ a2 << 1);
            byte b3 = (byte) (a3 >> 7 & 0x1B ^ a3 << 1);

            data[loffset + (3 - i)] = (byte) (b0 ^ a3 ^ a2 ^ b1 ^ a1);
            data[loffset + 16 + (3 - i)] = (byte) (b1 ^ a0 ^ a3 ^ b2 ^ a2);
            data[loffset + 32 + (3 - i)] = (byte) (b2 ^ a1 ^ a0 ^ b3 ^ a3);
            data[loffset + 48 + (3 - i)] = (byte) (b3 ^ a2 ^ a1 ^ b0 ^ a0);
        }
    }

    protected final void MixRowsZ(byte[] data, int loffset) {
        for (int i = 0; i < ROW_COUNT; ++i) {
            byte a0 = data[loffset + 16 * (3 - i)];
            byte a1 = data[loffset + 4 + 16 * (3 - i)];
            byte a2 = data[loffset + 8 + 16 * (3 - i)];
            byte a3 = data[loffset + 12 + 16 * (3 - i)];

            byte b0 = (byte) (a0 >> 7 & 0x1B ^ a0 << 1);
            byte b1 = (byte) (a1 >> 7 & 0x1B ^ a1 << 1);
            byte b2 = (byte) (a2 >> 7 & 0x1B ^ a2 << 1);
            byte b3 = (byte) (a3 >> 7 & 0x1B ^ a3 << 1);

            data[loffset + 16 * (3 - i)] = (byte) (b0 ^ a3 ^ a2 ^ b1 ^ a1);
            data[loffset + 4 + 16 * (3 - i)] = (byte) (b1 ^ a0 ^ a3 ^ b2 ^ a2);
            data[loffset + 8 + 16 * (3 - i)] = (byte) (b2 ^ a1 ^ a0 ^ b3 ^ a3);
            data[loffset + 12 + 16 * (3 - i)] = (byte) (b3 ^ a2 ^ a1 ^ b0 ^ a0);
        }
    }
    ///

    protected final void MixColsX(byte[] data, int loffset) {
        for (int i = 0; i < ROW_COUNT; ++i) {
            byte a0 = data[loffset + (3 - i) * 4 + 3];
            byte a1 = data[loffset + (3 - i) * 4 + 2];
            byte a2 = data[loffset + (3 - i) * 4 + 1];
            byte a3 = data[loffset + (3 - i) * 4 + 0];

            byte b0 = (byte) (a0 >> 7 & 0x1B ^ a0 << 1);
            byte b1 = (byte) (a1 >> 7 & 0x1B ^ a1 << 1);
            byte b2 = (byte) (a2 >> 7 & 0x1B ^ a2 << 1);
            byte b3 = (byte) (a3 >> 7 & 0x1B ^ a3 << 1);

            data[loffset + (3 - i) * 4 + 3] = (byte) (b0 ^ a3 ^ a2 ^ b1 ^ a1);
            data[loffset + (3 - i) * 4 + 2] = (byte) (b1 ^ a0 ^ a3 ^ b2 ^ a2);
            data[loffset + (3 - i) * 4 + 1] = (byte) (b2 ^ a1 ^ a0 ^ b3 ^ a3);
            data[loffset + (3 - i) * 4 + 0] = (byte) (b3 ^ a2 ^ a1 ^ b0 ^ a0);
        }
    }

    protected final void MixColsY(byte[] data, int loffset) {
        for (int i = 0; i < ROW_COUNT; ++i) {
            byte a0 = data[loffset + (3 - i) * 16 + 3];
            byte a1 = data[loffset + (3 - i) * 16 + 2];
            byte a2 = data[loffset + (3 - i) * 16 + 1];
            byte a3 = data[loffset + (3 - i) * 16 + 0];

            byte b0 = (byte) (a0 >> 7 & 0x1B ^ a0 << 1);
            byte b1 = (byte) (a1 >> 7 & 0x1B ^ a1 << 1);
            byte b2 = (byte) (a2 >> 7 & 0x1B ^ a2 << 1);
            byte b3 = (byte) (a3 >> 7 & 0x1B ^ a3 << 1);

            data[loffset + (3 - i) * 16 + 3] = (byte) (b0 ^ a3 ^ a2 ^ b1 ^ a1);
            data[loffset + (3 - i) * 16 + 2] = (byte) (b1 ^ a0 ^ a3 ^ b2 ^ a2);
            data[loffset + (3 - i) * 16 + 1] = (byte) (b2 ^ a1 ^ a0 ^ b3 ^ a3);
            data[loffset + (3 - i) * 16 + 0] = (byte) (b3 ^ a2 ^ a1 ^ b0 ^ a0);
        }
    }

    protected final void MixColsZ(byte[] data, int loffset) {
        for (int i = 0; i < ROW_COUNT; ++i) {
            byte a0 = data[loffset + (3 - i) * 4 + 48];
            byte a1 = data[loffset + (3 - i) * 4 + 32];
            byte a2 = data[loffset + (3 - i) * 4 + 16];
            byte a3 = data[loffset + (3 - i) * 4 + 0];

            byte b0 = (byte) (a0 >> 7 & 0x1B ^ a0 << 1);
            byte b1 = (byte) (a1 >> 7 & 0x1B ^ a1 << 1);
            byte b2 = (byte) (a2 >> 7 & 0x1B ^ a2 << 1);
            byte b3 = (byte) (a3 >> 7 & 0x1B ^ a3 << 1);

            data[loffset + (3 - i) * 4 + 48] = (byte) (b0 ^ a3 ^ a2 ^ b1 ^ a1);
            data[loffset + (3 - i) * 4 + 32] = (byte) (b1 ^ a0 ^ a3 ^ b2 ^ a2);
            data[loffset + (3 - i) * 4 + 16] = (byte) (b2 ^ a1 ^ a0 ^ b3 ^ a3);
            data[loffset + (3 - i) * 4 + 0] = (byte) (b3 ^ a2 ^ a1 ^ b0 ^ a0);
        }
    }

    protected final void MixLayersX(byte[] layer, int offset, int subkey) {
        for (int z = 0; z < LAYER_COUNT; ++z) {
            int loffset = offset + 16 * z;
            int lkoffset = 64 * subkey + 16 * z;

            SubBytesX(layer, loffset);
            MixRowsX(layer, loffset);
            MixColsX(layer, loffset);
            AddRoundSubKeyX(layer, loffset, lkoffset);
        }
    }

    protected final void MixLayersY(byte[] layer, int offset, int subkey) {
        for (int z = 0; z < LAYER_COUNT; ++z) {
            int loffset = offset + 4 * z;
            int lkoffset = 64 * subkey + 4 * z;

            SubBytesY(layer, loffset);
            MixRowsY(layer, loffset);
            MixColsY(layer, loffset);
            AddRoundSubKeyY(layer, loffset, lkoffset);
        }
    }

    protected final void MixLayersZ(byte[] layer, int offset, int subkey) {
        for (int z = 0; z < LAYER_COUNT; ++z) {
            int loffset = offset + z;
            int lkoffset = 64 * subkey + z;

            SubBytesZ(layer, loffset);
            MixRowsZ(layer, loffset);
            MixColsZ(layer, loffset);
            AddRoundSubKeyZ(layer, loffset, lkoffset);
        }
    }
}
