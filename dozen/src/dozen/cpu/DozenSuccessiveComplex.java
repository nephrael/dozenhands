/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dozen.cpu;


public class DozenSuccessiveComplex extends DozenComplex {

    public DozenSuccessiveComplex(byte[] key) {
        super(key);
    }
    
    public DozenSuccessiveComplex(byte[] key, byte[] iv) {
        super(key, iv);
    }
    
    public DozenSuccessiveComplex(byte[] key, long counter) {
        super(key, counter);
    }
    
    @Override
    public boolean encryptECB(byte[] data) {
        
        int blockTotal = data.length/BLOCK_SIZE;
        int processed = 0;

        while(processed < blockTotal)
            encryptBlock(data, (processed++)*BLOCK_SIZE);
        
        return true;
    }

    @Override
    protected boolean encryptCTR(byte[] data, long counterOffset) {
        int blockTotal = data.length/BLOCK_SIZE;
        int processed = 0;

        while(processed < blockTotal)
        {
            int offset = processed*BLOCK_SIZE;
            byte[] block = new byte[BLOCK_SIZE];
            for (int i = 0; i < BLOCK_SIZE; ++i)
                block[i] = (byte) (counterOffset + offset + i);
            
            encryptBlock(block, 0);

            for (int i = 0; i < BLOCK_SIZE; ++i)
                data[offset + i] ^= block[i];
            
            ++processed;            
        }
        return true;
    }
    
    protected boolean encryptBlock(byte[] block, int offset)
    {        
        AddRoundKey(block, offset);
        MixLayers(block, offset, 1, 1,4,16);
        MixLayers(block, offset, 2, 1,16,4);
        MixLayers(block, offset, 3, 16,4,1);
        
        return true;
    }
}
