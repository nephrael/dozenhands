/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dozen.cpu;

import dozen.Cypher;

public class DozenParallelComplex extends DozenComplex {

    private class Worker implements Runnable
    {
        private final byte[] data;
        private final int offset;
        private final Cypher.Mode mode;
        private final long counterOffset;
        
        Worker(byte[] data, int offset)
        {
            this.data = data;
            this.offset = offset;
            this.mode = Mode.ECB;
            this.counterOffset = 0;
        }
        
        
        Worker(byte[] data, int offset, long counter)
        {
            this.data = data;
            this.offset = offset;
            this.counterOffset = counter;
            this.mode = Mode.CTR;
        }
        
        @Override
        public void run() {
            switch(mode)
            {
                case ECB:
                    AddRoundKey(data, offset);
                    MixLayers(data, offset, 1, 1,4,16);
                    MixLayers(data, offset, 2, 1,16,4);
                    MixLayers(data, offset, 3, 16,4,1);
                    break;
                
                case CTR:
                    byte[] block = new byte[BLOCK_SIZE];
                    long co = counterOffset;
                    for (int i = 0; i < BLOCK_SIZE; ++i)
                        block[i] = (byte) (co + offset + i);
                    
                    AddRoundKey(block, 0);
                    MixLayers(block, 0, 1, 1,4,16);
                    MixLayers(block, 0, 2, 1,16,4);
                    MixLayers(block, 0, 3, 16,4,1);
                    
                    for (int i = 0; i < BLOCK_SIZE; ++i)
                        data[offset + i] ^= block[i];
                        
                    break;
            }
        }        
    }
    
    public DozenParallelComplex(byte[] key) {
        super(key);
    }
    
    public DozenParallelComplex(byte[] key, long counter) {
        super(key, counter);
    }
    
    @Override
    public boolean encryptECB(byte[] data) {
        ThreadPoolManager mng = new ThreadPoolManager();
        
        int size = data.length;
        int block_count = size/BLOCK_SIZE;
        for (int i=0; i<block_count; ++i)
            mng.addWorker(new Worker(data, i*BLOCK_SIZE));

        mng.join();
        return true;
    }
    
    @Override
    public boolean encryptCTR(byte[] data, long counterOffset) {
        ThreadPoolManager mng = new ThreadPoolManager();
        
        int size = data.length;
        int block_count = size/BLOCK_SIZE;
        for (int i=0; i<block_count; ++i)
            mng.addWorker(new Worker(data, i*BLOCK_SIZE, counterOffset));

        mng.join();
        return true;
    }
}
