/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dozen.cpu;

import dozen.Dozen;

/**
 *
 * @author d.kuzminskiy
 */
public abstract class DozenComplex extends Dozen {

    public DozenComplex(byte[] key) {
        super(key);
    }
    
    public DozenComplex(byte[] key, byte[] iv) {
        super(key, iv);
    }
    
    public DozenComplex(byte[] key, long counter) {
        super(key, counter);
    }
    
    protected final void AddRoundKey(byte[] block, int offset)
    {
        for (int i = 0; i< BLOCK_SIZE; i++)
            block[offset+i] ^= m_key[i]; 
    }
    
    protected final void AddRoundSubKey(byte[] data, int loffset, int lkoffset, int Xweight, int Yweight)
    {
        for(int i = 0; i < ROW_COUNT; ++i)
            for(int j = 0; j<COL_COUNT; ++j)
                data[getStatePosition(loffset, i,j, Xweight,Yweight)] ^= m_key[getStatePosition(lkoffset, i,j, Xweight,Yweight)]; 
    }
    
    protected final int getStatePosition(int offset, int x, int y, int Xweight, int Yweight)
    {
        return offset + Yweight*y + Xweight*(3-x);
    }
    
    protected final void SubBytes(byte[] data, int loffset, int Xweight, int Yweight)
    {
        for(int i = 0; i < ROW_COUNT; ++i)
            for(int j = 0; j < COL_COUNT; ++j)
            {
                int pos = getStatePosition(loffset, i, j, Xweight, Yweight);
                data[pos] = SBox[data[pos] & 0xFF];
            }
    }
    
    protected final byte MixValue(byte data)
    {
        return (byte) (data >> 7 & 0x1B ^ data << 1);
    }
    
    protected final void MixRows(byte[] data, int loffset, int Xweight, int Yweight)
    {
        for(int i = 0; i < ROW_COUNT; ++i)
        {
            byte[] a = {data[getStatePosition(loffset, i, 0, Xweight, Yweight)], 
                        data[getStatePosition(loffset, i, 1, Xweight, Yweight)],
                        data[getStatePosition(loffset, i, 2, Xweight, Yweight)],
                        data[getStatePosition(loffset, i, 3, Xweight, Yweight)]};
            
           data[getStatePosition(loffset, i, 0, Xweight, Yweight)] = (byte) (MixValue(a[0]) ^ a[3] ^ a[2] ^ MixValue(a[1]) ^ a[1]); 
           data[getStatePosition(loffset, i, 1, Xweight, Yweight)] = (byte) (MixValue(a[1]) ^ a[0] ^ a[3] ^ MixValue(a[2]) ^ a[2]); 
           data[getStatePosition(loffset, i, 2, Xweight, Yweight)] = (byte) (MixValue(a[2]) ^ a[1] ^ a[0] ^ MixValue(a[3]) ^ a[3]);
           data[getStatePosition(loffset, i, 3, Xweight, Yweight)] = (byte) (MixValue(a[3]) ^ a[2] ^ a[1] ^ MixValue(a[0]) ^ a[0]);
        }
    }
    
    protected final void MixCols(byte[] data, int loffset, int Xweight, int Yweight)
    {
        for(int i = 0; i < ROW_COUNT; ++i)
        {
            byte[] a = {data[getStatePosition(loffset, i, 3, Xweight, Yweight)], 
                        data[getStatePosition(loffset, i, 2, Xweight, Yweight)],
                        data[getStatePosition(loffset, i, 1, Xweight, Yweight)],
                        data[getStatePosition(loffset, i, 0, Xweight, Yweight)]};
            
           data[getStatePosition(loffset, i, 3, Xweight, Yweight)] = (byte) (MixValue(a[0]) ^ a[3] ^ a[2] ^ MixValue(a[1]) ^ a[1]); 
           data[getStatePosition(loffset, i, 2, Xweight, Yweight)] = (byte) (MixValue(a[1]) ^ a[0] ^ a[3] ^ MixValue(a[2]) ^ a[2]); 
           data[getStatePosition(loffset, i, 1, Xweight, Yweight)] = (byte) (MixValue(a[2]) ^ a[1] ^ a[0] ^ MixValue(a[3]) ^ a[3]);
           data[getStatePosition(loffset, i, 0, Xweight, Yweight)] = (byte) (MixValue(a[3]) ^ a[2] ^ a[1] ^ MixValue(a[0]) ^ a[0]);
        }
    }
    
    protected final void MixLayers(byte[] layer, int offset, int subkey, int Xweight, int Yweight, int Zweight)
    {
        for(int z = 0; z < LAYER_COUNT; ++z)
        {
            int loffset = offset + Zweight*z;
            int lkoffset = 64*subkey + Zweight*z;
            
            SubBytes(layer, loffset, Xweight, Yweight);
            MixRows(layer, loffset, Xweight, Yweight);
            MixCols(layer, loffset, Yweight, Xweight);
            AddRoundSubKey(layer, loffset, lkoffset, Xweight, Yweight);
        }
    }
}
