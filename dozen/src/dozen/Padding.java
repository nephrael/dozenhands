/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dozen;

/**
 *
 * @author d.kuzminskiy
 */
public interface Padding {
    public int calculate(int size, int blocksize);
    boolean pad(byte[] Array, int Offset, int PaddingSize, int Effective);
    int realSize(byte[] Array,  int blocksize);
}
