/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dozen;

import java.nio.ByteBuffer;
import java.util.UUID;

/**
 *
 * @author mail_000
 */
public class EncryptionData {
    UUID id;
    ByteBuffer data;
    byte[] key;
    long counter;

    public EncryptionData(ByteBuffer data, byte[] key, long counter, UUID id) {
        this.data = data;
        this.key = key;
        this.counter = counter;
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public long getCounter() {
        return counter;
    }

    public void setCounter(long counter) {
        this.counter = counter;
    }

    public ByteBuffer getData() {
        return data;
    }

    public void setData(ByteBuffer data) {
        this.data = data;
    }

    public byte[] getKey() {
        return key;
    }

    public void setKey(byte[] key) {
        this.key = key;
    }
}
