/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dozen.padding;

import dozen.Padding;
import java.util.Arrays;


public class ZeroPadding implements Padding {
    
    @Override
    public int calculate(int size, int blocksize)
    {
        return size % blocksize == 0 ? size : size + (blocksize - size % blocksize);
    }

    @Override
    public boolean pad(byte[] Data, int Offset, int DataSize, int EffectiveSize) {
        Arrays.fill(Data, Offset+EffectiveSize, Offset+DataSize, (byte)0);       
        return true;
    }

    @Override
    public int realSize(byte[] Array, int blocksize) {
        int count = Array.length;
        
        while(Array[count-1] == 0 && blocksize-- != 0)
            --count;
        
        return count;
    }
    
}
