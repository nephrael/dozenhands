/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dozen.padding;

import dozen.Padding;

/**
 *
 * @author d.kuzminskiy
 */
public class PKCS7 implements Padding {

    @Override
    public int calculate(int size, int blocksize) {
        return size % blocksize == 0 ? size : size + (blocksize - size % blocksize);
    }

    @Override
    public boolean pad(byte[] Array,  int Offset, int PaddingSize, int Effective) {
        byte count = (byte) (PaddingSize - Effective);
        for (int i=Offset+Effective; i<Offset+PaddingSize; ++i)
            Array[i] = count;        
        return true;
    }

    @Override
    public int realSize(byte[] Array, int blocksize) {
        int count = Array.length;
        int lastByte = Array[count-1] & 0xFF;
        
        if (lastByte >= blocksize)
            return count;
        
        boolean ok = true;
        for(int i=count-1; i>count-lastByte-1 && ok; --i)
            ok &= (i>0 && lastByte == Array[i]);
        
        if (ok)
            return count-lastByte;
        
        return count;
    }
    
}
